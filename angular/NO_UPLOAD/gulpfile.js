
let gulp = require('gulp');
let del = require('del');
let imageResize = require('gulp-image-resize');
let path = require('path');
let rename = require('gulp-rename');
let debug = require('gulp-debug');
let merge = require('merge-stream');

let INPUT_DIR = './NO_UPLOAD/img_src';
let OUTPUT_DIR = './img';


// Move up a level to allow del to work without removing the CWD safety net
process.chdir('..');

gulp.task('clean:images', function() {
    return del([ OUTPUT_DIR + '/*' ]);
});

gulp.task('resize:images', function() {

    let copied = copyImages({
        filenames: [
            'logo.jpg',
            'main-page.jpg',
            'facebook.svg',
            'instagram.svg',
            'pause.svg',
            'play.svg',
            'twitter.svg',
            'vimeo.svg',
            'youtube.svg',
            'loader.gif',
            // Does not resize animated gifs properly so just copy
            'works/manifestations/smurf-virtual.sml.gif',
            'works/manifestations/smurf-virtual.med.gif',
            'works/manifestations/smurf-virtual.lrg.gif'
        ]
    });

    let logos = resizeImages({
        variants: [
            { label: 'sml', width: 167 },
            { label: 'med', width: 333 },
            { label: 'lrg', width: 500 }
        ],
        filenames: [
            'works/manifestations-logo.png',
            'works/recursions-logo.png',
            'works/tower-logo.png',
        ]
    });

    let landscape = resizeImages({
        variants: [
            { label: 'sml', width: 500 },
            { label: 'med', width: 1000 },
            { label: 'lrg', width: 1500 }
        ],
        filenames: [
            'works/manifestations/congregation.jpg',
            'works/manifestations/congregation2.jpg',
            'works/manifestations/fish.jpg',
            'works/manifestations/fish-person.jpg',
            'works/manifestations/shard.jpg',
            'works/manifestations/shard-person.jpg',
            'works/manifestations/shard-person2.jpg',
            'works/manifestations/smurf.jpg',
            'works/manifestations/smurf-detail.jpg',
            'works/manifestations/smurf-person.jpg',
            'works/recursions/1.jpg',
            'works/recursions/2.jpg',
            'works/recursions/3.jpg',
            'works/recursions/4.jpg',
            'works/recursions/5.jpg',
            'works/recursions/6.jpg',
            'works/recursions/7.jpg',
            'works/recursions/8.jpg',
            'works/recursions/9.jpg',
            'works/recursions/10.jpg',
            'works/recursions/11.jpg',
            'works/recursions/12.jpg',
            'works/recursions/13.jpg',
            'works/recursions/14.jpg',
            'works/recursions/15.jpg',
            'works/recursions/16.jpg',
            'works/recursions/17.jpg',
            'works/recursions/18.jpg',
            'works/recursions/19.jpg',
            'works/recursions/20.jpg',
            'works/tower/interior.jpg',
            'works/tower/interior2.jpg',
            'works/tower/interior3.jpg',
            'works/tower/interior4.jpg',
            'works/tower/person.jpg',
            'works/tower/person2.jpg',
        ]
    })

    let portrait = resizeImages({
        variants: [
            { label: 'sml', width: 333 },
            { label: 'med', width: 666 },
            { label: 'lrg', width: 1000 }
        ],
        filenames: [
            'works/manifestations/preacher.jpg',
            'works/tower/1.jpg',
            'works/tower/2.jpg',
            'works/tower/3.jpg',
            'works/tower/4.jpg',
            'works/tower/5.jpg',
            'works/tower/6.jpg',
            'works/tower/7.jpg',
            'works/tower/8.jpg',
            'works/tower/9.jpg',
            'works/tower/10.jpg',
            'works/tower/11.jpg',
            'works/tower/12.jpg'
        ]
    })

    return merge([copied, logos, landscape, portrait])
});


function copyImages(data) {
    relFns = data.filenames.map((f) => path.join(INPUT_DIR, f));
    return gulp.src(relFns, { base: INPUT_DIR })
        .pipe(debug({ title: 'copied' }))
        .pipe(gulp.dest(OUTPUT_DIR));
}


function resizeImages(data) {
    relFns = data.filenames.map((f) => path.join(INPUT_DIR, f));
    
    let streams = data.variants.map((variant) => {
        return gulp.src(relFns, { base: INPUT_DIR })
            .pipe(debug({ title: 'resized [' + variant.label + ']' }))
            .pipe(imageResize({
                width: variant.width,
            }))
            .pipe(rename((path) => {
                path.basename += '.' + variant.label
            }))
            .pipe(gulp.dest(OUTPUT_DIR));
    });
    return merge(streams);
}