import { Component, OnInit, ContentChildren, QueryList, Input, AfterContentInit, forwardRef } from '@angular/core';
import { CarouselImageComponent } from './carousel-image.component';
import { CarouselDiptychComponent } from './carousel-diptych.component';
import { CarouselTriptychComponent } from './carousel-triptych.component';
import { CarouselItemBase } from './carousel-item-base';
import { Subject } from 'rxjs/Subject';

export type CarouselItemComponent = CarouselImageComponent | CarouselDiptychComponent | CarouselTriptychComponent;


@Component({
    moduleId: module.id,
    selector: 'carousel',
    template: `
    <ul class="carousel-nav">
        <li *ngFor="let item of items; let i = index" [class.active]="item.active">
            <a (click)="selectItem(i, $event)" href="#" [class.active]="i === currInd">{{ i + 1 }}</a>
        </li>
    </ul> 
    <div class="content-container">
        <div class="content">
            <ng-content></ng-content>
        </div>
    </div>
    <div class="spacer" [style.padding-top]="aspectRatio"></div>
    `,
    styles: [`
    ul.carousel-nav > li {
        display: inline-block;
        margin-right: 12px;
    }
    ul.carousel-nav {
        margin-bottom: 6px;
    }
    .content-container {
        position: relative;
    }
    .spacer {
        padding-top: 66.6666667%;
        display: block;
    }
    .content {
        /* background-color: black; */
        position: absolute;
        top: 0px;
        right: 0px;
        bottom: 0px;
        left: 0px;
        display: flex;
        flex-direction: column;
        justify-content: flex-start;
        align-items: center;
    }
    `]
})
export class CarouselComponent implements OnInit, AfterContentInit {

    @ContentChildren(CarouselItemBase) items: QueryList<CarouselItemBase>;
    @Input('aspectratio') aspectRatio: string = "66.666666667%";

    private contentLoadedSource = new Subject<null>();

    contentLoaded$ = this.contentLoadedSource.asObservable();

    currInd: number = 0;

    get defaultItem(): CarouselItemBase {
        return this.items.first;
    }

    constructor() { }

    ngOnInit() { }


    ngAfterContentInit() {
        // Select initial image
        let activeImgs = this.items.filter((item) => item.active);
        this.selectItem(activeImgs[0] || this.defaultItem);
        // Notify that content has loaded
        this.contentLoadedSource.complete();
    }

    selectItem(item: CarouselItemBase | number, event?: MouseEvent) {
        let isIndex = typeof item === "number";
        this.items.toArray().forEach((it, i) => {
            it.active = isIndex ? item === i : item === it;
            if (it.active) { this.currInd = i; }
        });
        // Prevent navigation if triggered from anchor element
        if (event) { event.preventDefault(); }
    }
}