"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var carousel_item_base_1 = require("./carousel-item-base");
var CarouselTriptychComponent = CarouselTriptychComponent_1 = (function (_super) {
    __extends(CarouselTriptychComponent, _super);
    function CarouselTriptychComponent() {
        return _super.call(this) || this;
    }
    CarouselTriptychComponent.prototype.ngOnInit = function () { };
    return CarouselTriptychComponent;
}(carousel_item_base_1.CarouselItemBase));
__decorate([
    core_1.Input("imgLeft"),
    __metadata("design:type", String)
], CarouselTriptychComponent.prototype, "imgSrcLeft", void 0);
__decorate([
    core_1.Input("altLeft"),
    __metadata("design:type", String)
], CarouselTriptychComponent.prototype, "altLeft", void 0);
__decorate([
    core_1.Input("imgMiddle"),
    __metadata("design:type", String)
], CarouselTriptychComponent.prototype, "imgSrcMiddle", void 0);
__decorate([
    core_1.Input("altMiddle"),
    __metadata("design:type", String)
], CarouselTriptychComponent.prototype, "altMiddle", void 0);
__decorate([
    core_1.Input("imgRight"),
    __metadata("design:type", String)
], CarouselTriptychComponent.prototype, "imgSrcRight", void 0);
__decorate([
    core_1.Input("altRight"),
    __metadata("design:type", String)
], CarouselTriptychComponent.prototype, "altRight", void 0);
CarouselTriptychComponent = CarouselTriptychComponent_1 = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'carousel-triptych',
        template: "\n        <div *ngIf=\"active\" class=\"root-container\">\n        <picture>\n            <source srcset=\"{{ imgSrcLeft | imageVariant:'lrg' }}\" media=\"(min-width: 1000px)\" />\n            <source srcset=\"{{ imgSrcLeft | imageVariant:'med' }}\" media=\"(min-width: 650px)\" />\n            <img src=\"{{ imgSrcLeft | imageVariant:'sml' }}\" alt=\"{{ altLeft }}\" />\n        </picture>\n        <picture>\n            <source srcset=\"{{ imgSrcMiddle | imageVariant:'lrg' }}\" media=\"(min-width: 1000px)\" />\n            <source srcset=\"{{ imgSrcMiddle | imageVariant:'med' }}\" media=\"(min-width: 650px)\" />\n            <img src=\"{{ imgSrcMiddle | imageVariant:'sml' }}\" alt=\"{{ altMiddle }}\" />\n        </picture>\n        <picture>\n            <source srcset=\"{{ imgSrcRight | imageVariant:'lrg' }}\" media=\"(min-width: 1000px)\" />\n            <source srcset=\"{{ imgSrcRight | imageVariant:'med' }}\" media=\"(min-width: 650px)\" />\n            <img src=\"{{ imgSrcRight | imageVariant:'sml' }}\" alt=\"{{ altRight }}\" />\n        </picture>\n    </div> \n    ",
        styles: ["\n        .root-container {\n            width: 100%;\n            display: flex;\n            align-items: center;\n            justify-content: center;\n        }\n        picture {\n            width: calc(33.3% - 4px);\n        }\n        picture > img {\n            width: 100%;\n        }\n        picture:nth-child(1) {\n            margin-right: 3px;\n        }\n        picture:nth-child(2) {\n            margin-left: 3px;\n            margin-right: 3px;\n        }\n        picture:nth-child(3) {\n            margin-left: 3px;\n        }\n    "],
        providers: [{ provide: carousel_item_base_1.CarouselItemBase, useExisting: core_1.forwardRef(function () { return CarouselTriptychComponent_1; }) }]
    }),
    __metadata("design:paramtypes", [])
], CarouselTriptychComponent);
exports.CarouselTriptychComponent = CarouselTriptychComponent;
var CarouselTriptychComponent_1;
//# sourceMappingURL=carousel-triptych.component.js.map