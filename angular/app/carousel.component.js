"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var carousel_item_base_1 = require("./carousel-item-base");
var Subject_1 = require("rxjs/Subject");
var CarouselComponent = (function () {
    function CarouselComponent() {
        this.aspectRatio = "66.666666667%";
        this.contentLoadedSource = new Subject_1.Subject();
        this.contentLoaded$ = this.contentLoadedSource.asObservable();
        this.currInd = 0;
    }
    Object.defineProperty(CarouselComponent.prototype, "defaultItem", {
        get: function () {
            return this.items.first;
        },
        enumerable: true,
        configurable: true
    });
    CarouselComponent.prototype.ngOnInit = function () { };
    CarouselComponent.prototype.ngAfterContentInit = function () {
        // Select initial image
        var activeImgs = this.items.filter(function (item) { return item.active; });
        this.selectItem(activeImgs[0] || this.defaultItem);
        // Notify that content has loaded
        this.contentLoadedSource.complete();
    };
    CarouselComponent.prototype.selectItem = function (item, event) {
        var _this = this;
        var isIndex = typeof item === "number";
        this.items.toArray().forEach(function (it, i) {
            it.active = isIndex ? item === i : item === it;
            if (it.active) {
                _this.currInd = i;
            }
        });
        // Prevent navigation if triggered from anchor element
        if (event) {
            event.preventDefault();
        }
    };
    return CarouselComponent;
}());
__decorate([
    core_1.ContentChildren(carousel_item_base_1.CarouselItemBase),
    __metadata("design:type", core_1.QueryList)
], CarouselComponent.prototype, "items", void 0);
__decorate([
    core_1.Input('aspectratio'),
    __metadata("design:type", String)
], CarouselComponent.prototype, "aspectRatio", void 0);
CarouselComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'carousel',
        template: "\n    <ul class=\"carousel-nav\">\n        <li *ngFor=\"let item of items; let i = index\" [class.active]=\"item.active\">\n            <a (click)=\"selectItem(i, $event)\" href=\"#\" [class.active]=\"i === currInd\">{{ i + 1 }}</a>\n        </li>\n    </ul> \n    <div class=\"content-container\">\n        <div class=\"content\">\n            <ng-content></ng-content>\n        </div>\n    </div>\n    <div class=\"spacer\" [style.padding-top]=\"aspectRatio\"></div>\n    ",
        styles: ["\n    ul.carousel-nav > li {\n        display: inline-block;\n        margin-right: 12px;\n    }\n    ul.carousel-nav {\n        margin-bottom: 6px;\n    }\n    .content-container {\n        position: relative;\n    }\n    .spacer {\n        padding-top: 66.6666667%;\n        display: block;\n    }\n    .content {\n        /* background-color: black; */\n        position: absolute;\n        top: 0px;\n        right: 0px;\n        bottom: 0px;\n        left: 0px;\n        display: flex;\n        flex-direction: column;\n        justify-content: flex-start;\n        align-items: center;\n    }\n    "]
    }),
    __metadata("design:paramtypes", [])
], CarouselComponent);
exports.CarouselComponent = CarouselComponent;
//# sourceMappingURL=carousel.component.js.map