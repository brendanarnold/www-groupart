"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var tab_component_1 = require("./tab.component");
var Subject_1 = require("rxjs/Subject");
var TabsComponent = (function () {
    function TabsComponent() {
        this.basePath = "/";
        this.contentLoadedSource = new Subject_1.Subject();
        this.contentLoaded$ = this.contentLoadedSource.asObservable();
    }
    Object.defineProperty(TabsComponent.prototype, "defaultTab", {
        get: function () {
            return this.tabs.first;
        },
        enumerable: true,
        configurable: true
    });
    TabsComponent.prototype.ngAfterContentInit = function () {
        // Select a tab
        var activeTabs = this.tabs.filter(function (tab) { return tab.active; });
        this.selectTab(activeTabs[0] || this.defaultTab);
        // Mark the loading as complete
        this.contentLoadedSource.complete();
    };
    TabsComponent.prototype.selectTab = function (tab) {
        if (typeof tab === "number") {
            this.tabs.toArray().forEach(function (t, i) { return t.active = i === tab; });
        }
        else {
            this.tabs.toArray().forEach(function (t) { return t.active = t === tab; });
        }
    };
    return TabsComponent;
}());
__decorate([
    core_1.ContentChildren(tab_component_1.TabComponent),
    __metadata("design:type", core_1.QueryList)
], TabsComponent.prototype, "tabs", void 0);
__decorate([
    core_1.Input('basePath'),
    __metadata("design:type", String)
], TabsComponent.prototype, "basePath", void 0);
TabsComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'tabs',
        template: "\n    <nav>\n        <ul class=\"tab-nav-container\">\n            <li *ngFor=\"let tab of tabs\" (click)=\"selectTab(tab)\" [class.active]=\"tab.active\">\n                <a routerLink=\"{{ basePath }}{{ tab.tabId }}\" routerLinkActive=\"active\">{{ tab.title }}</a>\n            </li>\n        </ul> \n    </nav>\n    <div class=\"tab-container\">\n        <ng-content></ng-content>\n    </div>\n    ",
        styles: [
            "\n        :host {\n            position: relative;\n        }\n        .tab-nav-container {\n            list-style-type: none;\n            margin-bottom: 30px;\n        }\n        .tab-nav-container > li {\n            display: inline-block;\n            margin-right: 12px;\n        }\n        .tab-container {\n\n        }\n        "
        ]
    }),
    __metadata("design:paramtypes", [])
], TabsComponent);
exports.TabsComponent = TabsComponent;
//# sourceMappingURL=tabs.component.js.map