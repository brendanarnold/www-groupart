
import { Component, OnInit, OnDestroy, trigger, style, state, transition, animate, Inject } from '@angular/core';
import { I_GESTURE, IGesture } from './services/gesture/igesture';
import { I_LOGGER, ILogger } from './services/logger/ilogger';
import { Link } from './dropdown-menu.component';


@Component({
    moduleId: module.id,
    selector: 'app-navigation',
    templateUrl: 'navigation.component.html',
    styleUrls: [ 'navigation.component.css' ],
    animations: [
        trigger('navState', [
            state('hidden', style({
                transform: 'translateY(-3rem)'
            })),
            state('visible', style({
                transform: 'translateY(0rem)'
            })),
            transition('hidden => visible', animate('300ms ease-in-out')),
            transition('visible => hidden', animate('300ms ease-in-out'))

        ])
    ]
})
export class NavigationComponent {


    public lockState: boolean = false;

    private _visibleState: VisibilityState = "visible";
    set visibleState(val: VisibilityState) {
        if (this._visibleState !== val) {
            this._visibleState = val;
            this.logger.trace("Nav visibility set to " + val);
        }
    }
    get visibleState() { return this._visibleState; }

    
    constructor(@Inject(I_LOGGER) private logger: ILogger) {

    }

    toggleVisibility() {
        if (this.visibleState === "hidden") {
            this.show() 
        } else {
            this.hide();
        }
    }    

    show() {
        if (this.lockState) { return; }
        this.visibleState = "visible";        
    }

    hide() {
        if (this.lockState) { return; }
        this.visibleState = "hidden";
    }

}

type VisibilityState = "visible" | "hidden";