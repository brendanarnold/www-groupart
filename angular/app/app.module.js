"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
require("./rxjs-extensions");
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var app_component_1 = require("./app.component");
var home_component_1 = require("./home/home.component");
var works_component_1 = require("./works/works.component");
var works_tower_component_1 = require("./works/works-tower.component");
var works_manifestations_component_1 = require("./works/works-manifestations.component");
var works_recursions_component_1 = require("./works/works-recursions.component");
var about_component_1 = require("./about/about.component");
var about_manifesto_component_1 = require("./about/about-manifesto.component");
var navigation_component_1 = require("./navigation.component");
var dropdown_menu_component_1 = require("./dropdown-menu.component");
var audio_player_component_1 = require("./audio-player.component");
var ilogger_1 = require("./services/logger/ilogger");
var console_logger_service_1 = require("./services/logger/console-logger.service");
var iaudio_1 = require("./services/audio/iaudio");
var html_audio_service_1 = require("./services/audio/html-audio.service");
var html_gesture_service_1 = require("./services/gesture/html-gesture.service");
var igesture_1 = require("./services/gesture/igesture");
var app_routing_module_1 = require("./app-routing.module");
var news_module_1 = require("./news/news.module");
var tab_component_1 = require("./tab.component");
var tabs_component_1 = require("./tabs.component");
var carousel_component_1 = require("./carousel.component");
var carousel_image_component_1 = require("./carousel-image.component");
var carousel_diptych_component_1 = require("./carousel-diptych.component");
var carousel_triptych_component_1 = require("./carousel-triptych.component");
var image_variant_pipe_1 = require("./image-variant.pipe");
var mediacheck_service_1 = require("./services/mediacheck/mediacheck.service");
var phplist_subscribe_service_1 = require("./services/subscribe/phplist-subscribe.service");
var isubscribe_1 = require("./services/subscribe/isubscribe");
var test_component_1 = require("./test.component");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            news_module_1.NewsModule,
            platform_browser_1.BrowserModule,
            app_routing_module_1.AppRoutingModule,
            forms_1.FormsModule,
            http_1.HttpModule
        ],
        declarations: [
            app_component_1.AppComponent,
            home_component_1.HomeComponent,
            works_component_1.WorksComponent,
            works_tower_component_1.WorksTowerComponent,
            works_manifestations_component_1.WorksManifestationsComponent,
            works_recursions_component_1.WorksRecursionsComponent,
            about_component_1.AboutComponent,
            about_manifesto_component_1.AboutManifestoComponent,
            audio_player_component_1.AudioPlayerComponent,
            navigation_component_1.NavigationComponent,
            dropdown_menu_component_1.DropdownMenuComponent,
            tab_component_1.TabComponent,
            tabs_component_1.TabsComponent,
            carousel_component_1.CarouselComponent,
            carousel_image_component_1.CarouselImageComponent,
            carousel_diptych_component_1.CarouselDiptychComponent,
            carousel_triptych_component_1.CarouselTriptychComponent,
            image_variant_pipe_1.ImageVariantPipe,
            test_component_1.TestComponent
        ],
        exports: [],
        providers: [
            { provide: ilogger_1.I_LOGGER, useClass: console_logger_service_1.ConsoleLogger },
            { provide: iaudio_1.I_AUDIO, useClass: html_audio_service_1.HtmlAudio },
            { provide: igesture_1.I_GESTURE, useClass: html_gesture_service_1.HtmlGesture },
            { provide: isubscribe_1.I_SUBSCRIBE, useClass: phplist_subscribe_service_1.PhpListSubscribeService },
            platform_browser_1.Title,
            mediacheck_service_1.MediacheckService
        ],
        bootstrap: [app_component_1.AppComponent]
    }),
    __metadata("design:paramtypes", [])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map