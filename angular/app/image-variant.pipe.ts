
import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: "imageVariant" })
export class ImageVariantPipe implements PipeTransform {
    transform(value: string, variant: string) {
        let els = value.split('.');
        els.splice(els.length - 1, 0, variant);
        return els.join('.');
    }

}