import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'test',
    template: `

<div class="page-container">
<main>
     <h1>Test Area</h1>

     <email-form></email-form> 
   </main>
</div>
    `,
})
export class TestComponent implements OnInit {
    constructor() { }

    ngOnInit() { }

}