"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var iaudio_1 = require("./services/audio/iaudio");
var ilogger_1 = require("./services/logger/ilogger");
var audio_clips_1 = require("./services/audio/audio-clips");
var PLACEHOLDER_TIME = "00:00";
var AudioPlayerComponent = (function () {
    function AudioPlayerComponent(audioService, logger) {
        var _this = this;
        this.audioService = audioService;
        this.logger = logger;
        this.subscriptions = [];
        this.playPauseString = "Play";
        this.clipName = null;
        this.lockState = false;
        this._visibleState = "visible";
        this.progressTime = PLACEHOLDER_TIME;
        this.totalTime = PLACEHOLDER_TIME;
        this.subscriptions.push(audioService.playbackPaused$.subscribe(function () { _this.pause(); }));
        this.subscriptions.push(audioService.playbackStopped$.subscribe(function () { _this.stop(); }));
        this.subscriptions.push(audioService.playbackStarted$.subscribe(function (clip) { _this.play(); }));
        this.subscriptions.push(this.audioService.playbackChanged$.subscribe(function () {
            _this.playPauseString = (_this.audioService.isPlaying) ? "Pause" : "Play";
        }));
        this.subscriptions.push(this.audioService.playbackTimeChanged$.subscribe(function () {
            _this.progressTime = _this.secondsToPlaybackTimeString(_this.audioService.playbackTime);
        }));
        this.subscriptions.push(this.audioService.newClipLoaded$.subscribe(function () {
            _this.loadNewAudio();
            _this.newClipLoaded();
        }));
        //this.subscriptions.push(this.audioService.newClipLoaded$.subscribe(() => { this.newClipLoaded(); }));
    }
    Object.defineProperty(AudioPlayerComponent.prototype, "visibleState", {
        get: function () { return this._visibleState; },
        set: function (val) {
            if (this._visibleState !== val) {
                this._visibleState = val;
                this.logger.trace("Nav visibility set to " + val);
            }
        },
        enumerable: true,
        configurable: true
    });
    AudioPlayerComponent.prototype.newClipLoaded = function () {
        var clip = this.audioService.currentClip;
        if (clip) {
            this.totalTime = this.secondsToPlaybackTimeString(clip.duration);
            this.clipName = clip.name;
            this.logger.trace("Clip loaded: " + clip.filename);
        }
        else {
            this.totalTime = PLACEHOLDER_TIME;
        }
    };
    AudioPlayerComponent.prototype.secondsToPlaybackTimeString = function (t) {
        var mins = Math.floor(t / 60);
        var secs = Math.floor(t % 60);
        var secsStr = secs < 10 ? "0" + secs : "" + secs;
        return mins + ":" + secsStr;
    };
    AudioPlayerComponent.prototype.togglePlayback = function () {
        this.audioService.togglePlayback(audio_clips_1.AUDIO_CLIPS.manifesto);
    };
    AudioPlayerComponent.prototype.playManifesto = function () {
        this.audioService.play(audio_clips_1.AUDIO_CLIPS.manifesto);
    };
    AudioPlayerComponent.prototype.loadNewAudio = function () {
        if (this.audioElement.nativeElement.stop)
            this.audioElement.nativeElement.stop();
        this.audioElement.nativeElement.src = this.audioService.currentClip.filename;
    };
    AudioPlayerComponent.prototype.play = function () {
        this.audioElement.nativeElement.play();
    };
    AudioPlayerComponent.prototype.pause = function () {
        if (this.audioElement.nativeElement.pause)
            this.audioElement.nativeElement.pause();
    };
    AudioPlayerComponent.prototype.stop = function () {
        if (this.audioElement.nativeElement.stop)
            this.audioElement.nativeElement.stop();
    };
    AudioPlayerComponent.prototype.timeUpdated = function () {
        this.audioService.timeUpdated(this.audioElement.nativeElement.currentTime);
    };
    AudioPlayerComponent.prototype.toggleVisibility = function () {
        if (this.visibleState === "hidden") {
            this.show();
        }
        else {
            this.hide();
        }
    };
    AudioPlayerComponent.prototype.show = function () {
        if (this.lockState) {
            return;
        }
        this.visibleState = "visible";
    };
    AudioPlayerComponent.prototype.hide = function () {
        if (this.lockState) {
            return;
        }
        this.visibleState = "hidden";
    };
    AudioPlayerComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var sub = _a[_i];
            sub.unsubscribe();
        }
    };
    return AudioPlayerComponent;
}());
__decorate([
    core_1.ViewChild('audioelement'),
    __metadata("design:type", Object)
], AudioPlayerComponent.prototype, "audioElement", void 0);
AudioPlayerComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: "audio-player",
        templateUrl: 'audio-player.component.html',
        styleUrls: ['audio-player.component.css'],
        animations: [
            core_1.trigger('navState', [
                core_1.state('hidden', core_1.style({
                    transform: 'translateY(3rem)'
                })),
                core_1.state('visible', core_1.style({
                    transform: 'translateY(0rem)'
                })),
                core_1.transition('hidden => visible', core_1.animate('300ms ease-in-out')),
                core_1.transition('visible => hidden', core_1.animate('300ms ease-in-out'))
            ])
        ]
    }),
    __param(0, core_1.Inject(iaudio_1.I_AUDIO)),
    __param(1, core_1.Inject(ilogger_1.I_LOGGER)),
    __metadata("design:paramtypes", [Object, Object])
], AudioPlayerComponent);
exports.AudioPlayerComponent = AudioPlayerComponent;
//# sourceMappingURL=audio-player.component.js.map