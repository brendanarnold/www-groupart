
import { Component, ViewChild, Inject, OnDestroy, trigger, state, style, animate, transition } from '@angular/core';
import { I_AUDIO, IAudio } from './services/audio/iaudio';
import { I_LOGGER, ILogger } from './services/logger/ilogger';
import { AudioClip } from './services/audio/audioclip';
import { Subscription } from 'rxjs/Subscription';
import { AUDIO_CLIPS } from './services/audio/audio-clips';

const PLACEHOLDER_TIME = "00:00";

@Component({
    moduleId: module.id,
    selector: "audio-player",
    templateUrl: 'audio-player.component.html',
    styleUrls: [ 'audio-player.component.css'], 
    animations: [
        trigger('navState', [
            state('hidden', style({
                transform: 'translateY(3rem)'
            })),
            state('visible', style({
                transform: 'translateY(0rem)'
            })),
            transition('hidden => visible', animate('300ms ease-in-out')),
            transition('visible => hidden', animate('300ms ease-in-out'))

        ])
    ]
})
export class AudioPlayerComponent implements OnDestroy {

    @ViewChild('audioelement') audioElement;
    subscriptions: Subscription[] = [];
    playPauseString: string = "Play";
    clipName: string = null;
    public lockState: boolean = false;

    private _visibleState: VisibilityState = "visible";
    set visibleState(val: VisibilityState) {
        if (this._visibleState !== val) {
            this._visibleState = val;
            this.logger.trace("Nav visibility set to " + val);
        }
    }
    get visibleState() { return this._visibleState; }

    progressTime = PLACEHOLDER_TIME;
    totalTime = PLACEHOLDER_TIME;

    constructor(@Inject(I_AUDIO) private audioService: IAudio,
        @Inject(I_LOGGER) private logger: ILogger) {

        this.subscriptions.push(audioService.playbackPaused$.subscribe(() => { this.pause(); }));
        this.subscriptions.push(audioService.playbackStopped$.subscribe(() => { this.stop(); }));
        this.subscriptions.push(audioService.playbackStarted$.subscribe(clip => { this.play(); }));
        this.subscriptions.push(this.audioService.playbackChanged$.subscribe(() => {
            this.playPauseString = (this.audioService.isPlaying) ? "Pause" : "Play";
        }));
        this.subscriptions.push(this.audioService.playbackTimeChanged$.subscribe(() => {
            this.progressTime = this.secondsToPlaybackTimeString(this.audioService.playbackTime);
        }));
        this.subscriptions.push(this.audioService.newClipLoaded$.subscribe(() => { 
            this.loadNewAudio();
            this.newClipLoaded();
        }));
        //this.subscriptions.push(this.audioService.newClipLoaded$.subscribe(() => { this.newClipLoaded(); }));
    }


    newClipLoaded() {
        let clip = this.audioService.currentClip;
        if (clip) {
            this.totalTime = this.secondsToPlaybackTimeString(clip.duration);
            this.clipName = clip.name;
            this.logger.trace("Clip loaded: " + clip.filename);
        } else {
            this.totalTime = PLACEHOLDER_TIME;
        }
    }

    secondsToPlaybackTimeString(t: number) {
        let mins = Math.floor(t / 60);
        let secs = Math.floor(t % 60)
        let secsStr = secs < 10 ? "0" + secs : "" + secs;
        return mins + ":" + secsStr;
    }

    togglePlayback() {
        this.audioService.togglePlayback(AUDIO_CLIPS.manifesto);
    }

    playManifesto() {
        this.audioService.play(AUDIO_CLIPS.manifesto);
    }

    loadNewAudio() {
        if (this.audioElement.nativeElement.stop) this.audioElement.nativeElement.stop();
        this.audioElement.nativeElement.src = this.audioService.currentClip.filename;
    }

    play() {
        this.audioElement.nativeElement.play();
    }

    pause() {
        if (this.audioElement.nativeElement.pause) this.audioElement.nativeElement.pause();
    }

    stop() {
        if (this.audioElement.nativeElement.stop) this.audioElement.nativeElement.stop();
    }

    timeUpdated() {
        this.audioService.timeUpdated(this.audioElement.nativeElement.currentTime);
    }


    toggleVisibility() {
        if (this.visibleState === "hidden") {
            this.show() 
        } else {
            this.hide();
        }
    }    

    show() {
        if (this.lockState) { return; }
        this.visibleState = "visible";        
    }

    hide() {
        if (this.lockState) { return; }
        this.visibleState = "hidden";
    }


    ngOnDestroy() {
        for (let sub of this.subscriptions) { sub.unsubscribe(); }
    }

}

type VisibilityState = "visible" | "hidden";