"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var ilogger_1 = require("./services/logger/ilogger");
var NavigationComponent = (function () {
    function NavigationComponent(logger) {
        this.logger = logger;
        this.lockState = false;
        this._visibleState = "visible";
    }
    Object.defineProperty(NavigationComponent.prototype, "visibleState", {
        get: function () { return this._visibleState; },
        set: function (val) {
            if (this._visibleState !== val) {
                this._visibleState = val;
                this.logger.trace("Nav visibility set to " + val);
            }
        },
        enumerable: true,
        configurable: true
    });
    NavigationComponent.prototype.toggleVisibility = function () {
        if (this.visibleState === "hidden") {
            this.show();
        }
        else {
            this.hide();
        }
    };
    NavigationComponent.prototype.show = function () {
        if (this.lockState) {
            return;
        }
        this.visibleState = "visible";
    };
    NavigationComponent.prototype.hide = function () {
        if (this.lockState) {
            return;
        }
        this.visibleState = "hidden";
    };
    return NavigationComponent;
}());
NavigationComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'app-navigation',
        templateUrl: 'navigation.component.html',
        styleUrls: ['navigation.component.css'],
        animations: [
            core_1.trigger('navState', [
                core_1.state('hidden', core_1.style({
                    transform: 'translateY(-3rem)'
                })),
                core_1.state('visible', core_1.style({
                    transform: 'translateY(0rem)'
                })),
                core_1.transition('hidden => visible', core_1.animate('300ms ease-in-out')),
                core_1.transition('visible => hidden', core_1.animate('300ms ease-in-out'))
            ])
        ]
    }),
    __param(0, core_1.Inject(ilogger_1.I_LOGGER)),
    __metadata("design:paramtypes", [Object])
], NavigationComponent);
exports.NavigationComponent = NavigationComponent;
//# sourceMappingURL=navigation.component.js.map