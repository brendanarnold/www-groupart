import { Component, Input, OnInit, forwardRef } from '@angular/core';
import { CarouselItemBase } from './carousel-item-base';

@Component({
    moduleId: module.id,
    selector: 'carousel-triptych',
    template: `
        <div *ngIf="active" class="root-container">
        <picture>
            <source srcset="{{ imgSrcLeft | imageVariant:'lrg' }}" media="(min-width: 1000px)" />
            <source srcset="{{ imgSrcLeft | imageVariant:'med' }}" media="(min-width: 650px)" />
            <img src="{{ imgSrcLeft | imageVariant:'sml' }}" alt="{{ altLeft }}" />
        </picture>
        <picture>
            <source srcset="{{ imgSrcMiddle | imageVariant:'lrg' }}" media="(min-width: 1000px)" />
            <source srcset="{{ imgSrcMiddle | imageVariant:'med' }}" media="(min-width: 650px)" />
            <img src="{{ imgSrcMiddle | imageVariant:'sml' }}" alt="{{ altMiddle }}" />
        </picture>
        <picture>
            <source srcset="{{ imgSrcRight | imageVariant:'lrg' }}" media="(min-width: 1000px)" />
            <source srcset="{{ imgSrcRight | imageVariant:'med' }}" media="(min-width: 650px)" />
            <img src="{{ imgSrcRight | imageVariant:'sml' }}" alt="{{ altRight }}" />
        </picture>
    </div> 
    `,
    styles: [`
        .root-container {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
        }
        picture {
            width: calc(33.3% - 4px);
        }
        picture > img {
            width: 100%;
        }
        picture:nth-child(1) {
            margin-right: 3px;
        }
        picture:nth-child(2) {
            margin-left: 3px;
            margin-right: 3px;
        }
        picture:nth-child(3) {
            margin-left: 3px;
        }
    `],
    providers: [{ provide: CarouselItemBase, useExisting: forwardRef(() => CarouselTriptychComponent) }]
})
export class CarouselTriptychComponent extends CarouselItemBase implements OnInit {

    @Input("imgLeft") imgSrcLeft: string;
    @Input("altLeft") altLeft: string;
    @Input("imgMiddle") imgSrcMiddle: string;
    @Input("altMiddle") altMiddle: string;
    @Input("imgRight") imgSrcRight: string;
    @Input("altRight") altRight: string;

    constructor() { super(); }

    ngOnInit() { }
}