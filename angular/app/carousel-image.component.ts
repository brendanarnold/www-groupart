import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { CarouselItemBase } from './carousel-item-base';

@Component({
    moduleId: module.id,
    selector: 'carousel-image',
    template: `
    <div *ngIf="active" class="root-container">
        <picture>
            <source srcset="{{ imageUrl | imageVariant:'lrg' }}" media="(min-width: 1000px)" />
            <source srcset="{{ imageUrl | imageVariant:'med' }}" media="(min-width: 650px)" />
            <img src="{{ imageUrl | imageVariant:'sml' }}" alt="{{ alt }}" />
        </picture>
    </div>
    `,
    styles: [`
    .root-container {
        width: 100%;
    }
    picture {
        width: 100%;
    } 
    img {
        width: 100%;
    }
    `],
    providers: [{ provide: CarouselItemBase, useExisting: forwardRef(() => CarouselImageComponent) }]
})
export class CarouselImageComponent extends CarouselItemBase implements OnInit {
    @Input('img') imageUrl: string;
    @Input('alt') title: string;

    constructor() { super(); }

    ngOnInit() { 
    }
}