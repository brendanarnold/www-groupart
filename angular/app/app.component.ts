import { Component, Inject, Renderer, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { I_GESTURE, IGesture } from './services/gesture/igesture';
import { I_LOGGER, ILogger } from './services/logger/ilogger';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { MediacheckService } from './services/mediacheck/mediacheck.service';
import { NavigationComponent } from './navigation.component';
import { AudioPlayerComponent } from './audio-player.component';
import 'rxjs/add/operator/bufferCount';

@Component({
    selector: 'my-app',
    template: `
        <div id="layout-container" (mousemove)="mouseMove($event)" (click)="click($event)">
            <router-outlet></router-outlet>
            <app-navigation (mouseenter)="mouseEnterDecoration()" (mouseleave)="mouseLeaveDecoration()"></app-navigation>
            <audio-player (mouseenter)="mouseEnterDecoration()" (mouseleave)="mouseLeaveDecoration()"></audio-player>
        </div>
    `,
    styleUrls: ['app/app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

    subscriptions: Subscription[] = [];
    isMouseOverDecoration = false;
    lingerTime = 2500;
    windowScrollDirectionObservable: Observable<number>;

    @ViewChild(NavigationComponent) appNav: NavigationComponent;
    @ViewChild(AudioPlayerComponent) audioPlayer: AudioPlayerComponent;

    constructor(@Inject(I_GESTURE) private gesture: IGesture, 
        @Inject(I_LOGGER) private logger: ILogger,
        private renderer: Renderer,
        private mediacheck: MediacheckService ) {

        // Hook in the source for global window scrolling service
        this.renderer.listenGlobal('window', 'scroll', (ev) => { this.gesture.windowScrolled(ev); });
        // Now consume service to show/hide decorations (i.e. navigation and audio UI)
        this.subscriptions.push(this.gesture.mouseMoved$.subscribe(() => { 
            this.appNav.show();
            this.audioPlayer.show();
            }));
        this.subscriptions.push(this.gesture.mouseMoved$.debounceTime(this.lingerTime).subscribe(() => { 
            if (!this.isMouseOverDecoration) { 
                this.appNav.hide(); 
                this.audioPlayer.hide();
            } 
        }));
        // Set up the windowScrollDirectionObservable since we want to consume streams of dY
        this.windowScrollDirectionObservable = this.gesture.windowScrolled$
            .map(ev => window.pageYOffset )
            .bufferCount(2, 1)
            .map(y => y[0] - y[1] );
        // Consume the streams of dY
        this.subscriptions.push(this.windowScrollDirectionObservable
            .filter(dy => dy > 0)
            .subscribe(dy => { 
                this.appNav.show();
                this.audioPlayer.show();
            }));
        this.subscriptions.push(this.windowScrollDirectionObservable
            .filter(dy => dy > 0)
            .debounceTime(this.lingerTime)
            .subscribe(dy => { 
                if (window.pageYOffset === 0) { return; }
                this.appNav.hide();
                this.audioPlayer.hide();
            }));
        this.subscriptions.push(this.windowScrollDirectionObservable
            .filter(dy => dy < 0)
            .subscribe(dy => {
                this.appNav.hide();
                this.audioPlayer.hide();
            }));



    }

    mouseEnterDecoration() {
        this.isMouseOverDecoration = true;
    }

    mouseLeaveDecoration() {
        this.isMouseOverDecoration = false;
    }

    ngOnInit() {
        // Use the media check service to listen for when CSS media queries are applied 
        this.mediacheck.onMqChange('small', () => this.configSmallScreen());
        this.mediacheck.onMqChange('large', () => this.configLargeScreen());
        if (this.mediacheck.check('small')) { this.configSmallScreen(); }
        if (this.mediacheck.check('large')) { this.configLargeScreen(); }
    }

    // Want to hide decorations after a short amount of time when on small screen
    configSmallScreen() {
        this.appNav.lockState = false;
        this.audioPlayer.lockState = false;
        this.logger.trace("Configured for small screen");
    }

    // Pin decorations to page when on large screen
    configLargeScreen() {
        this.appNav.show();
        this.audioPlayer.show();
        this.appNav.lockState = true;
        this.audioPlayer.lockState = true;
        this.logger.trace("Configured for large screen");
    }


    ngOnDestroy() {
        this.subscriptions.forEach(sub => sub.unsubscribe());
    }

    mouseMove(event) {
        this.gesture.mouseMoved(event);        
    }

    click(event) {
       this.gesture.appClicked(event);
    }

}