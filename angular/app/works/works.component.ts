import { Component, OnInit, OnDestroy, AfterContentInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Params } from '@angular/router';
import { TabsComponent } from '../tabs.component';
import { Subscription } from 'rxjs/Subscription';


@Component({
    moduleId: module.id,
    selector: 'works',
    templateUrl: 'works.component.html',
    styleUrls: ['works.component.css'],
})
export class WorksComponent implements OnInit {

    private subscriptions: Subscription[] = [];

    constructor(private titleService: Title, private route: ActivatedRoute) { }


    ngOnInit() {
        this.titleService.setTitle("Works | The Orm");
    }


}