import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
    moduleId: module.id,
    selector: 'works-tower',
    templateUrl: 'works-tower.component.html',
    styleUrls: ['works-common.component.css', 'works-tower.component.css']
})
export class WorksTowerComponent implements OnInit {
    constructor(private titleService: Title) { }

    ngOnInit() { 
        this.titleService.setTitle("Tower | The Orm");
    }
}