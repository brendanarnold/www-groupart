import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
    moduleId: module.id,
    selector: 'works-recursions',
    templateUrl: 'works-recursions.component.html',
    styleUrls: ['works-common.component.css', 'works-recursions.component.css']
})
export class WorksRecursionsComponent implements OnInit {
    constructor(private titleService: Title) { }

    ngOnInit() { 
        this.titleService.setTitle("Recursions | The Orm");
    }
}