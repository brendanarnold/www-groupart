import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
    moduleId: module.id,
    selector: 'works-manifestations',
    templateUrl: 'works-manifestations.component.html',
    styleUrls: ['works-common.component.css', 'works-manifestations.component.css']
})
export class WorksManifestationsComponent implements OnInit {
    constructor(private titleService: Title) { }


    ngOnInit() { 
        this.titleService.setTitle("Manifestations | The Orm");
    }
}