"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var WorksManifestationsComponent = (function () {
    function WorksManifestationsComponent(titleService) {
        this.titleService = titleService;
    }
    WorksManifestationsComponent.prototype.ngOnInit = function () {
        this.titleService.setTitle("Manifestations | The Orm");
    };
    return WorksManifestationsComponent;
}());
WorksManifestationsComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'works-manifestations',
        templateUrl: 'works-manifestations.component.html',
        styleUrls: ['works-common.component.css', 'works-manifestations.component.css']
    }),
    __metadata("design:paramtypes", [platform_browser_1.Title])
], WorksManifestationsComponent);
exports.WorksManifestationsComponent = WorksManifestationsComponent;
//# sourceMappingURL=works-manifestations.component.js.map