"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var carousel_item_base_1 = require("./carousel-item-base");
var CarouselDiptychComponent = CarouselDiptychComponent_1 = (function (_super) {
    __extends(CarouselDiptychComponent, _super);
    function CarouselDiptychComponent() {
        return _super.call(this) || this;
    }
    CarouselDiptychComponent.prototype.ngAfterContentInit = function () {
        //     w1 / ar1 = w2 / ar2
        //     w1 * (ar2 / ar1) = w2
        //     w1 + w2 = 100
        //     w1 * (1 + ar2 / ar1) = 100
        //     w1 = 100 / (1 + ar2 / ar1)
        //     w2 = 100 - w1
        var w1 = 100.0 / (1 + this.aspectRatioRight / this.aspectRatioLeft);
        var w2 = 100.0 - w1;
        this.leftWidth = w1;
        this.rightWidth = w2;
    };
    return CarouselDiptychComponent;
}(carousel_item_base_1.CarouselItemBase));
__decorate([
    core_1.Input("imgLeft"),
    __metadata("design:type", String)
], CarouselDiptychComponent.prototype, "imgSrcLeft", void 0);
__decorate([
    core_1.Input("altLeft"),
    __metadata("design:type", String)
], CarouselDiptychComponent.prototype, "altLeft", void 0);
__decorate([
    core_1.Input("aspectRatioLeft"),
    __metadata("design:type", Number)
], CarouselDiptychComponent.prototype, "aspectRatioLeft", void 0);
__decorate([
    core_1.Input("imgRight"),
    __metadata("design:type", String)
], CarouselDiptychComponent.prototype, "imgSrcRight", void 0);
__decorate([
    core_1.Input("altRight"),
    __metadata("design:type", String)
], CarouselDiptychComponent.prototype, "altRight", void 0);
__decorate([
    core_1.Input("aspectRatioRight"),
    __metadata("design:type", Number)
], CarouselDiptychComponent.prototype, "aspectRatioRight", void 0);
CarouselDiptychComponent = CarouselDiptychComponent_1 = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'carousel-diptych',
        template: "\n    <div *ngIf=\"active\" class=\"root-container\">\n        <picture [style.width]=\"leftWidth + '%'\">\n            <source srcset=\"{{ imgSrcLeft | imageVariant:'lrg' }}\" media=\"(min-width: 1000px)\" />\n            <source srcset=\"{{ imgSrcLeft | imageVariant:'med' }}\" media=\"(min-width: 650px)\" />\n            <img src=\"{{ imgSrcLeft | imageVariant:'sml' }}\" alt=\"{{ altLeft }}\" />\n        </picture>\n        <picture [style.width]=\"rightWidth + '%'\">\n            <source srcset=\"{{ imgSrcRight | imageVariant:'lrg' }}\" media=\"(min-width: 1000px)\" />\n            <source srcset=\"{{ imgSrcRight | imageVariant:'med' }}\" media=\"(min-width: 650px)\" />\n            <img src=\"{{ imgSrcRight | imageVariant:'sml' }}\" alt=\"{{ altRight }}\" />\n        </picture>\n    </div> \n    ",
        styles: ["\n        .root-container {\n            width: 100%;\n            display: flex;\n            flex-direction: row;\n            justify-content: center;\n            align-items: center;\n        }\n        picture:nth-child(1) {\n            margin-right: 3px;\n        }\n        picture:nth-child(2) {\n            margin-left: 3px;\n        }\n        picture > img {\n            max-width: 100%;\n        }\n    "],
        providers: [{ provide: carousel_item_base_1.CarouselItemBase, useExisting: core_1.forwardRef(function () { return CarouselDiptychComponent_1; }) }]
    }),
    __metadata("design:paramtypes", [])
], CarouselDiptychComponent);
exports.CarouselDiptychComponent = CarouselDiptychComponent;
var CarouselDiptychComponent_1;
//# sourceMappingURL=carousel-diptych.component.js.map