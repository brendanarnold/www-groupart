import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { WorksComponent } from './works/works.component';
import { WorksTowerComponent } from './works/works-tower.component';
import { WorksManifestationsComponent } from './works/works-manifestations.component';
import { WorksRecursionsComponent } from './works/works-recursions.component';
import { AboutComponent } from './about/about.component';
import { AboutManifestoComponent } from './about/about-manifesto.component';
import { TestComponent } from './test.component';

const appRoutes: Routes = [
        {
            path: 'home',
            component: HomeComponent
        },
        {
            path: '',
            redirectTo: '/home',
            pathMatch: 'full'
        },
        {
            path: 'works',
            component: WorksComponent
        },
        {
            path: 'works/recursions',
            component: WorksRecursionsComponent
        },
        {
            path: 'works/tower',
            component: WorksTowerComponent
        },
        {
            path: 'works/manifestations',
            component: WorksManifestationsComponent
        },
        {
            path: 'about',
            component: AboutComponent
        },
        {
            path: 'about/manifesto',
            component: AboutManifestoComponent
        },
        {
            path: 'test',
            component: TestComponent
        }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }