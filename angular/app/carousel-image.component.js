"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var carousel_item_base_1 = require("./carousel-item-base");
var CarouselImageComponent = CarouselImageComponent_1 = (function (_super) {
    __extends(CarouselImageComponent, _super);
    function CarouselImageComponent() {
        return _super.call(this) || this;
    }
    CarouselImageComponent.prototype.ngOnInit = function () {
    };
    return CarouselImageComponent;
}(carousel_item_base_1.CarouselItemBase));
__decorate([
    core_1.Input('img'),
    __metadata("design:type", String)
], CarouselImageComponent.prototype, "imageUrl", void 0);
__decorate([
    core_1.Input('alt'),
    __metadata("design:type", String)
], CarouselImageComponent.prototype, "title", void 0);
CarouselImageComponent = CarouselImageComponent_1 = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'carousel-image',
        template: "\n    <div *ngIf=\"active\" class=\"root-container\">\n        <picture>\n            <source srcset=\"{{ imageUrl | imageVariant:'lrg' }}\" media=\"(min-width: 1000px)\" />\n            <source srcset=\"{{ imageUrl | imageVariant:'med' }}\" media=\"(min-width: 650px)\" />\n            <img src=\"{{ imageUrl | imageVariant:'sml' }}\" alt=\"{{ alt }}\" />\n        </picture>\n    </div>\n    ",
        styles: ["\n    .root-container {\n        width: 100%;\n    }\n    picture {\n        width: 100%;\n    } \n    img {\n        width: 100%;\n    }\n    "],
        providers: [{ provide: carousel_item_base_1.CarouselItemBase, useExisting: core_1.forwardRef(function () { return CarouselImageComponent_1; }) }]
    }),
    __metadata("design:paramtypes", [])
], CarouselImageComponent);
exports.CarouselImageComponent = CarouselImageComponent;
var CarouselImageComponent_1;
//# sourceMappingURL=carousel-image.component.js.map