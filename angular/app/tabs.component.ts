import { Component, AfterContentInit, ContentChildren, QueryList, Input } from '@angular/core';
import { TabComponent } from './tab.component';
import { Subject } from 'rxjs/Subject';

@Component({
    moduleId: module.id,
    selector: 'tabs',
    template: `
    <nav>
        <ul class="tab-nav-container">
            <li *ngFor="let tab of tabs" (click)="selectTab(tab)" [class.active]="tab.active">
                <a routerLink="{{ basePath }}{{ tab.tabId }}" routerLinkActive="active">{{ tab.title }}</a>
            </li>
        </ul> 
    </nav>
    <div class="tab-container">
        <ng-content></ng-content>
    </div>
    `,
    styles: [
        `
        :host {
            position: relative;
        }
        .tab-nav-container {
            list-style-type: none;
            margin-bottom: 30px;
        }
        .tab-nav-container > li {
            display: inline-block;
            margin-right: 12px;
        }
        .tab-container {

        }
        `
    ]
})
export class TabsComponent implements AfterContentInit {

    @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;
    @Input('basePath') basePath: string = "/";

    private contentLoadedSource = new Subject<null>();

    contentLoaded$ = this.contentLoadedSource.asObservable();

    get defaultTab(): TabComponent {
        return this.tabs.first;
    }

    constructor() { }

    ngAfterContentInit() { 
        // Select a tab
        let activeTabs = this.tabs.filter((tab) => tab.active);
        this.selectTab(activeTabs[0] || this.defaultTab)
        // Mark the loading as complete
        this.contentLoadedSource.complete();
    }


    selectTab(tab: TabComponent | number) {
        if (typeof tab === "number") {
            this.tabs.toArray().forEach((t, i) => t.active = i === tab);
        } else {
            this.tabs.toArray().forEach((t) => t.active = t === tab);
        }
    }

}