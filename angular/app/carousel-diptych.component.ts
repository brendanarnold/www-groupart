import { Component, Input, forwardRef, AfterContentInit, ViewChild, ElementRef } from '@angular/core';
import { CarouselItemBase } from './carousel-item-base';

@Component({
    moduleId: module.id,
    selector: 'carousel-diptych',
    template: `
    <div *ngIf="active" class="root-container">
        <picture [style.width]="leftWidth + '%'">
            <source srcset="{{ imgSrcLeft | imageVariant:'lrg' }}" media="(min-width: 1000px)" />
            <source srcset="{{ imgSrcLeft | imageVariant:'med' }}" media="(min-width: 650px)" />
            <img src="{{ imgSrcLeft | imageVariant:'sml' }}" alt="{{ altLeft }}" />
        </picture>
        <picture [style.width]="rightWidth + '%'">
            <source srcset="{{ imgSrcRight | imageVariant:'lrg' }}" media="(min-width: 1000px)" />
            <source srcset="{{ imgSrcRight | imageVariant:'med' }}" media="(min-width: 650px)" />
            <img src="{{ imgSrcRight | imageVariant:'sml' }}" alt="{{ altRight }}" />
        </picture>
    </div> 
    `,
    styles: [`
        .root-container {
            width: 100%;
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
        }
        picture:nth-child(1) {
            margin-right: 3px;
        }
        picture:nth-child(2) {
            margin-left: 3px;
        }
        picture > img {
            max-width: 100%;
        }
    `],
    providers: [{ provide: CarouselItemBase, useExisting: forwardRef(() => CarouselDiptychComponent) }]
})
export class CarouselDiptychComponent extends CarouselItemBase implements AfterContentInit {

    @Input("imgLeft") imgSrcLeft: string;
    @Input("altLeft") altLeft: string;
    @Input("aspectRatioLeft") aspectRatioLeft: number;
    @Input("imgRight") imgSrcRight: string;
    @Input("altRight") altRight: string;
    @Input("aspectRatioRight") aspectRatioRight: number;

    leftWidth: number;
    rightWidth: number;

    constructor() { super(); }
    
    ngAfterContentInit() {
        //     w1 / ar1 = w2 / ar2
        //     w1 * (ar2 / ar1) = w2
        //     w1 + w2 = 100
        //     w1 * (1 + ar2 / ar1) = 100
        //     w1 = 100 / (1 + ar2 / ar1)
        //     w2 = 100 - w1
        let w1 = 100.0 / (1 + this.aspectRatioRight / this.aspectRatioLeft);
        let w2 = 100.0 - w1;
        this.leftWidth = w1;
        this.rightWidth = w2;
    }


}