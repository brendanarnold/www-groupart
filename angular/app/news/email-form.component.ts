import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { I_SUBSCRIBE, ISubscribe } from '../services/subscribe/isubscribe';
import { Subscription } from 'rxjs/Subscription';

@Component({
    moduleId: module.id,
    selector: 'email-form',
    templateUrl: 'email-form.component.html',
    styleUrls: [ 'email-form.component.css' ]
})
export class EmailFormComponent implements OnInit {

    @ViewChild('emailinput') emailInput;
    email = "";
    state: "ready" | "submitted" | "error" = "ready";
    subscriptions: Subscription[] = [];

    constructor(@Inject(I_SUBSCRIBE) private subscribe: ISubscribe) { }

    ngOnInit() { 
        
    }

    submitForm() {
        console.log("Submitted");

        this.subscriptions.push(this.subscribe.subscribeToMailingList(this.email)
            .subscribe(
                () => { this.state = "submitted" },  
                (err) => { 
                    this.state = "error";
                    console.log(err)
                }
        ));
    }

    resetForm() {
        this.emailInput.nativeElement.value = "";
        this.state = "ready";
        console.log("Form Reset")
    }
}