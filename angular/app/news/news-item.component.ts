import { Component, Input, ViewChild, ElementRef, Renderer, ViewEncapsulation } from '@angular/core';
import { NewsItem } from './services/news/newsitem';

@Component({
    moduleId: module.id,
    selector: 'news-item',
    templateUrl: 'news-item.component.html',
    styleUrls: ['news-item.component.css' ],
    encapsulation: ViewEncapsulation.None
})
export class NewsItemComponent {

    @Input() newsItem: NewsItem;

    constructor(private renderer: Renderer) { }

}