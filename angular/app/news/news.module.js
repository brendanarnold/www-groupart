"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var news_detail_component_1 = require("./news-detail.component");
var news_list_component_1 = require("./news-list.component");
var news_item_component_1 = require("./news-item.component");
var news_routing_module_1 = require("./news-routing.module");
var static_news_service_1 = require("./services/news/static-news.service");
var inews_1 = require("./services/news/inews");
var email_form_component_1 = require("./email-form.component");
var NewsModule = (function () {
    function NewsModule() {
    }
    return NewsModule;
}());
NewsModule = __decorate([
    core_1.NgModule({
        imports: [
            common_1.CommonModule,
            forms_1.FormsModule,
            news_routing_module_1.NewsRoutingModule,
        ],
        declarations: [
            news_detail_component_1.NewsDetailComponent,
            news_list_component_1.NewsListComponent,
            news_item_component_1.NewsItemComponent,
            email_form_component_1.EmailFormComponent
        ],
        exports: [
            email_form_component_1.EmailFormComponent
        ],
        providers: [
            { provide: inews_1.I_NEWS, useClass: static_news_service_1.StaticNews }
        ]
    }),
    __metadata("design:paramtypes", [])
], NewsModule);
exports.NewsModule = NewsModule;
//# sourceMappingURL=news.module.js.map