"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var platform_browser_1 = require("@angular/platform-browser");
var ilogger_1 = require("../services/logger/ilogger");
var inews_1 = require("./services/news/inews");
require("rxjs/add/operator/switchMap");
var NewsDetailComponent = (function () {
    function NewsDetailComponent(titleService, logger, news, route, router) {
        this.titleService = titleService;
        this.logger = logger;
        this.news = news;
        this.route = route;
        this.router = router;
        this.prevIsEnabled = false;
        this.nextIsEnabled = true;
        this.newsItem = null;
        this.subscriptions = [];
    }
    NewsDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Load the new items
        this.news.getAllNews()
            .then(function (newsItems) {
            // Check on initial load using a snapshot
            var id = _this.route.snapshot.params["id"];
            var ind = newsItems.findIndex(function (newsItem) { return newsItem.getId() === id; });
            var currNewsInd = ind && ind !== -1 ? ind : 0;
            _this.newsItem = newsItems[currNewsInd] || null;
            // Update the enabled/disabled status of navigation
            _this.prevIsEnabled = newsItems.length && currNewsInd !== 0;
            _this.nextIsEnabled = newsItems.length && currNewsInd < newsItems.length - 1;
            // Update the title if needed
            _this.titleService.setTitle((_this.newsItem ? _this.newsItem.title + " | " : "") + "The Orm");
            _this.logger.trace("Updated news item");
        });
    };
    NewsDetailComponent.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (s) { s.unsubscribe(); });
    };
    return NewsDetailComponent;
}());
NewsDetailComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'news-detail',
        templateUrl: 'news-detail.component.html',
        styleUrls: ['news-detail.component.css']
    }),
    __param(1, core_1.Inject(ilogger_1.I_LOGGER)),
    __param(2, core_1.Inject(inews_1.I_NEWS)),
    __metadata("design:paramtypes", [platform_browser_1.Title, Object, Object, router_1.ActivatedRoute,
        router_1.Router])
], NewsDetailComponent);
exports.NewsDetailComponent = NewsDetailComponent;
//# sourceMappingURL=news-detail.component.js.map