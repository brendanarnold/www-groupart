"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var isubscribe_1 = require("../services/subscribe/isubscribe");
var EmailFormComponent = (function () {
    function EmailFormComponent(subscribe) {
        this.subscribe = subscribe;
        this.email = "";
        this.state = "ready";
        this.subscriptions = [];
    }
    EmailFormComponent.prototype.ngOnInit = function () {
    };
    EmailFormComponent.prototype.submitForm = function () {
        var _this = this;
        console.log("Submitted");
        this.subscriptions.push(this.subscribe.subscribeToMailingList(this.email)
            .subscribe(function () { _this.state = "submitted"; }, function (err) {
            _this.state = "error";
            console.log(err);
        }));
    };
    EmailFormComponent.prototype.resetForm = function () {
        this.emailInput.nativeElement.value = "";
        this.state = "ready";
        console.log("Form Reset");
    };
    return EmailFormComponent;
}());
__decorate([
    core_1.ViewChild('emailinput'),
    __metadata("design:type", Object)
], EmailFormComponent.prototype, "emailInput", void 0);
EmailFormComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'email-form',
        templateUrl: 'email-form.component.html',
        styleUrls: ['email-form.component.css']
    }),
    __param(0, core_1.Inject(isubscribe_1.I_SUBSCRIBE)),
    __metadata("design:paramtypes", [Object])
], EmailFormComponent);
exports.EmailFormComponent = EmailFormComponent;
//# sourceMappingURL=email-form.component.js.map