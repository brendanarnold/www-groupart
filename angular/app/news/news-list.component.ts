import { Component, OnInit, OnDestroy, Inject, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { I_LOGGER, ILogger } from '../services/logger/ilogger';
import { I_NEWS, INews } from './services/news/inews';
import { NewsItem } from './services/news/newsitem';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/switchMap';

let NEWS_ITEMS_PER_PAGE = 10;

@Component({
    moduleId: module.id,
    selector: 'news-list',
    templateUrl: 'news-list.component.html',
    styleUrls: ['news-list.component.css']
})
export class NewsListComponent implements OnInit {
    constructor(private titleService: Title, 
                    @Inject(I_LOGGER) private logger: ILogger,
                    @Inject(I_NEWS) private news: INews) { }


    olderIsEnabled: boolean = false;
    newerIsEnabled: boolean = true;
    newsItems: NewsItem[] = [];

    subscriptions: Subscription[] = [];

    ngOnInit() {
        // Set title
        this.titleService.setTitle("News | The Orm");
        // Load the new items
        this.news.getAllNews()
            .then((newsItems) => { 
                this.newsItems = newsItems;
            });
    }



    ngOnDestroy() {
        this.subscriptions.forEach((s) => { s.unsubscribe(); });
    }

}