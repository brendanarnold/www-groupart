"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var ilogger_1 = require("../services/logger/ilogger");
var inews_1 = require("./services/news/inews");
require("rxjs/add/operator/switchMap");
var NEWS_ITEMS_PER_PAGE = 10;
var NewsListComponent = (function () {
    function NewsListComponent(titleService, logger, news) {
        this.titleService = titleService;
        this.logger = logger;
        this.news = news;
        this.olderIsEnabled = false;
        this.newerIsEnabled = true;
        this.newsItems = [];
        this.subscriptions = [];
    }
    NewsListComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Set title
        this.titleService.setTitle("News | The Orm");
        // Load the new items
        this.news.getAllNews()
            .then(function (newsItems) {
            _this.newsItems = newsItems;
        });
    };
    NewsListComponent.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (s) { s.unsubscribe(); });
    };
    return NewsListComponent;
}());
NewsListComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'news-list',
        templateUrl: 'news-list.component.html',
        styleUrls: ['news-list.component.css']
    }),
    __param(1, core_1.Inject(ilogger_1.I_LOGGER)),
    __param(2, core_1.Inject(inews_1.I_NEWS)),
    __metadata("design:paramtypes", [platform_browser_1.Title, Object, Object])
], NewsListComponent);
exports.NewsListComponent = NewsListComponent;
//# sourceMappingURL=news-list.component.js.map