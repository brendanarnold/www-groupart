import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { NewsDetailComponent }   from './news-detail.component';
import { NewsListComponent }   from './news-list.component';
import { NewsItemComponent } from './news-item.component';
import { NewsRoutingModule } from './news-routing.module';
import { StaticNews } from './services/news/static-news.service';
import { I_NEWS, INews } from './services/news/inews';
import { EmailFormComponent } from './email-form.component';
import { AppModule } from '../app.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NewsRoutingModule,
    ],
    declarations: [
        NewsDetailComponent,
        NewsListComponent,
        NewsItemComponent,
        EmailFormComponent
    ],
    exports: [
        EmailFormComponent
    ],
    providers: [
        { provide: I_NEWS, useClass: StaticNews }
    ]
})
export class NewsModule { }
