import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewsDetailComponent } from './news-detail.component';
import { NewsListComponent } from './news-list.component';

const routes: Routes = [
    { path: 'news', component: NewsListComponent },
    { path: 'news/:id', component: NewsDetailComponent }  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewsRoutingModule { }
