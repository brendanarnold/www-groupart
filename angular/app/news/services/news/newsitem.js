"use strict";
var NewsItem = (function () {
    function NewsItem() {
        this.publishedDate = new Date();
        this.title = "";
        this.bodyHtml = "";
    }
    NewsItem.prototype.setPublishedDate = function (val) {
        this.publishedDate = val;
        return this;
    };
    NewsItem.prototype.setTitle = function (val) {
        this.title = val;
        return this;
    };
    NewsItem.prototype.setBodyHtml = function (val) {
        this.bodyHtml = val;
        return this;
    };
    NewsItem.prototype.getId = function () {
        var yr = this.publishedDate.getUTCFullYear();
        var mt = this.publishedDate.getUTCMonth();
        var dy = this.publishedDate.getUTCDate();
        var hr = this.publishedDate.getUTCHours();
        var mn = this.publishedDate.getUTCMinutes();
        return yr + "-" + mt + "-" + dy + "_" + hr + "-" + mn;
    };
    return NewsItem;
}());
exports.NewsItem = NewsItem;
//# sourceMappingURL=newsitem.js.map