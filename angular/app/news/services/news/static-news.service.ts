import { Injectable } from '@angular/core';
import { INews } from './inews';
import { NewsItem } from './newsitem';

@Injectable()
export class StaticNews implements INews {

    constructor() { }

    getAllNews(): Promise<NewsItem[]> {
        return new Promise((resolve, reject) => {
            resolve(this.NEWS_ITEMS);
        });
    }

    private readonly NEWS_ITEMS: NewsItem[] = [
        new NewsItem()
            .setTitle("New year, new website")
            .setBodyHtml(`
            <p>We are pleased to launch the Orm's new website. We have included more text for context and made the design more accessible. We also wanted the audio discussions front and center and so they can be accessed from anywhere on the site and will continue to play as you switch pages.</p>

            <p>We hope you like the new designs and welcome any feedback at <a href="mailto:contact@theorm.space">contact@theorm.space</a>.</p>
            `)
            .setPublishedDate(new Date(2017, 1, 8)),
    ];





}