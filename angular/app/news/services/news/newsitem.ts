export class NewsItem {
    publishedDate: Date = new Date();
    title: string = "";
    bodyHtml: string = "";

    setPublishedDate(val: Date) {
        this.publishedDate = val;
        return this;
    }

    setTitle(val: string) {
        this.title = val;
        return this;
    }

    setBodyHtml(val: string) {
        this.bodyHtml = val;
        return this;
    }

    getId() {
        let yr = this.publishedDate.getUTCFullYear();
        let mt = this.publishedDate.getUTCMonth();
        let dy = this.publishedDate.getUTCDate();
        let hr = this.publishedDate.getUTCHours();
        let mn = this.publishedDate.getUTCMinutes();
        return `${yr}-${mt}-${dy}_${hr}-${mn}`;
    }
}