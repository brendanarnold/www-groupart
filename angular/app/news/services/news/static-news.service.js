"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var newsitem_1 = require("./newsitem");
var StaticNews = (function () {
    function StaticNews() {
        this.NEWS_ITEMS = [
            new newsitem_1.NewsItem()
                .setTitle("New year, new website")
                .setBodyHtml("\n            <p>We are pleased to launch the Orm's new website. We have included more text for context and made the design more accessible. We also wanted the audio discussions front and center and so they can be accessed from anywhere on the site and will continue to play as you switch pages.</p>\n\n            <p>We hope you like the new designs and welcome any feedback at <a href=\"mailto:contact@theorm.space\">contact@theorm.space</a>.</p>\n            ")
                .setPublishedDate(new Date(2017, 1, 8)),
        ];
    }
    StaticNews.prototype.getAllNews = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            resolve(_this.NEWS_ITEMS);
        });
    };
    return StaticNews;
}());
StaticNews = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], StaticNews);
exports.StaticNews = StaticNews;
//# sourceMappingURL=static-news.service.js.map