
import { OpaqueToken } from '@angular/core';
import { NewsItem } from './newsitem';
import { Observable } from 'rxjs/Observable';

export interface INews {
    getAllNews(): Promise<NewsItem[]>;
}

export let I_NEWS = new OpaqueToken('INews');