import { Component, OnInit, OnDestroy, Inject, ViewChild } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { I_LOGGER, ILogger } from '../services/logger/ilogger';
import { I_NEWS, INews } from './services/news/inews';
import { NewsItem } from './services/news/newsitem';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/switchMap';

@Component({
    moduleId: module.id,
    selector: 'news-detail',
    templateUrl: 'news-detail.component.html',
    styleUrls: ['news-detail.component.css']
})
export class NewsDetailComponent implements OnInit {
    constructor(private titleService: Title, 
                    @Inject(I_LOGGER) private logger: ILogger,
                    @Inject(I_NEWS) private news: INews,
                    private route: ActivatedRoute,
                    private router: Router) { }

    prevIsEnabled: boolean = false;
    nextIsEnabled: boolean = true;
    newsItem: NewsItem = null;

    subscriptions: Subscription[] = [];

    ngOnInit() {
        // Load the new items
        this.news.getAllNews()
            .then((newsItems) => { 
                // Check on initial load using a snapshot
                let id = this.route.snapshot.params["id"];
                let ind = newsItems.findIndex((newsItem) => newsItem.getId() === id);
                let currNewsInd = ind && ind !== -1 ? ind : 0;
                this.newsItem = newsItems[currNewsInd] || null;
                // Update the enabled/disabled status of navigation
                this.prevIsEnabled = newsItems.length && currNewsInd !== 0; 
                this.nextIsEnabled = newsItems.length && currNewsInd < newsItems.length - 1; 
                // Update the title if needed
                this.titleService.setTitle((this.newsItem ? this.newsItem.title + " | " : "") + "The Orm");
                this.logger.trace("Updated news item");
            });
    } 




    ngOnDestroy() {
        this.subscriptions.forEach((s) => { s.unsubscribe(); });
    }

}