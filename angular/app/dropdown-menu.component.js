"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var igesture_1 = require("./services/gesture/igesture");
var Link = (function () {
    function Link(route, text) {
        if (route === void 0) { route = ""; }
        if (text === void 0) { text = ""; }
        this.route = route;
        this.text = text;
    }
    return Link;
}());
exports.Link = Link;
var DropdownMenuComponent = (function () {
    function DropdownMenuComponent(gesture) {
        var _this = this;
        this.gesture = gesture;
        this.isVisible = false;
        this.subscriptions = [];
        this.text = "";
        this.links = [];
        this.subscriptions.push(this.gesture.appClicked$.subscribe(function (event) { return _this.appClicked(event); }));
    }
    DropdownMenuComponent.prototype.appClicked = function (event) {
        // If click outside of the menu when open, close the menu
        if (this.isVisible && !this.root.nativeElement.contains(event.target)) {
            this.isVisible = false;
        }
    };
    DropdownMenuComponent.prototype.mouseLeave = function () {
        //this.isVisible = false;
    };
    DropdownMenuComponent.prototype.toggleVisibility = function () {
        this.isVisible = !this.isVisible;
    };
    DropdownMenuComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var sub = _a[_i];
            sub.unsubscribe();
        }
    };
    return DropdownMenuComponent;
}());
__decorate([
    core_1.ViewChild("root"),
    __metadata("design:type", Object)
], DropdownMenuComponent.prototype, "root", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], DropdownMenuComponent.prototype, "text", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], DropdownMenuComponent.prototype, "links", void 0);
DropdownMenuComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'dropdown-menu',
        styles: ["\n    .dropdown-menu {\n    }\n    .title-container {\n        display: inline-block;\n    }\n    .dropdown-anchor {\n        position: relative;\n        height: 0px;\n    }\n    ul {\n        position: absolute;\n        top: 10px;\n        left: -10px;\n        background-color: #fff;\n        list-style-type: none;\n        padding-bottom: 10px;\n    }\n    ul > li {\n        padding: 10px 10px 0px 10px;\n    }\n    ul > li > a {\n    }\n    .isopen {\n        color: red;\n    }\n    "],
        template: "\n<div class=\"dropdown-menu\" #root>\n    <a [class.isopen]=\"isVisible\" class=\"hyperlink title-container\" (click)=\"toggleVisibility()\">{{ text }}</a>\n    <div (mouseleave)=\"mouseLeave()\" class=\"dropdown-anchor\">\n        <ul *ngIf=\"isVisible\" >\n            <li *ngFor=\"let link of links\"><a class=\"hyperlink\" routerLinkActive=\"active\" routerLink=\"{{ link.route }}\">{{ link.text }}</a></li>\n        </ul>\n    </div>\n </div>"
    }),
    __param(0, core_1.Inject(igesture_1.I_GESTURE)),
    __metadata("design:paramtypes", [Object])
], DropdownMenuComponent);
exports.DropdownMenuComponent = DropdownMenuComponent;
//# sourceMappingURL=dropdown-menu.component.js.map