"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var igesture_1 = require("./services/gesture/igesture");
var ilogger_1 = require("./services/logger/ilogger");
var mediacheck_service_1 = require("./services/mediacheck/mediacheck.service");
var navigation_component_1 = require("./navigation.component");
var audio_player_component_1 = require("./audio-player.component");
require("rxjs/add/operator/bufferCount");
var AppComponent = (function () {
    function AppComponent(gesture, logger, renderer, mediacheck) {
        var _this = this;
        this.gesture = gesture;
        this.logger = logger;
        this.renderer = renderer;
        this.mediacheck = mediacheck;
        this.subscriptions = [];
        this.isMouseOverDecoration = false;
        this.lingerTime = 2500;
        // Hook in the source for global window scrolling service
        this.renderer.listenGlobal('window', 'scroll', function (ev) { _this.gesture.windowScrolled(ev); });
        // Now consume service to show/hide decorations (i.e. navigation and audio UI)
        this.subscriptions.push(this.gesture.mouseMoved$.subscribe(function () {
            _this.appNav.show();
            _this.audioPlayer.show();
        }));
        this.subscriptions.push(this.gesture.mouseMoved$.debounceTime(this.lingerTime).subscribe(function () {
            if (!_this.isMouseOverDecoration) {
                _this.appNav.hide();
                _this.audioPlayer.hide();
            }
        }));
        // Set up the windowScrollDirectionObservable since we want to consume streams of dY
        this.windowScrollDirectionObservable = this.gesture.windowScrolled$
            .map(function (ev) { return window.pageYOffset; })
            .bufferCount(2, 1)
            .map(function (y) { return y[0] - y[1]; });
        // Consume the streams of dY
        this.subscriptions.push(this.windowScrollDirectionObservable
            .filter(function (dy) { return dy > 0; })
            .subscribe(function (dy) {
            _this.appNav.show();
            _this.audioPlayer.show();
        }));
        this.subscriptions.push(this.windowScrollDirectionObservable
            .filter(function (dy) { return dy > 0; })
            .debounceTime(this.lingerTime)
            .subscribe(function (dy) {
            if (window.pageYOffset === 0) {
                return;
            }
            _this.appNav.hide();
            _this.audioPlayer.hide();
        }));
        this.subscriptions.push(this.windowScrollDirectionObservable
            .filter(function (dy) { return dy < 0; })
            .subscribe(function (dy) {
            _this.appNav.hide();
            _this.audioPlayer.hide();
        }));
    }
    AppComponent.prototype.mouseEnterDecoration = function () {
        this.isMouseOverDecoration = true;
    };
    AppComponent.prototype.mouseLeaveDecoration = function () {
        this.isMouseOverDecoration = false;
    };
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Use the media check service to listen for when CSS media queries are applied 
        this.mediacheck.onMqChange('small', function () { return _this.configSmallScreen(); });
        this.mediacheck.onMqChange('large', function () { return _this.configLargeScreen(); });
        if (this.mediacheck.check('small')) {
            this.configSmallScreen();
        }
        if (this.mediacheck.check('large')) {
            this.configLargeScreen();
        }
    };
    // Want to hide decorations after a short amount of time when on small screen
    AppComponent.prototype.configSmallScreen = function () {
        this.appNav.lockState = false;
        this.audioPlayer.lockState = false;
        this.logger.trace("Configured for small screen");
    };
    // Pin decorations to page when on large screen
    AppComponent.prototype.configLargeScreen = function () {
        this.appNav.show();
        this.audioPlayer.show();
        this.appNav.lockState = true;
        this.audioPlayer.lockState = true;
        this.logger.trace("Configured for large screen");
    };
    AppComponent.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (sub) { return sub.unsubscribe(); });
    };
    AppComponent.prototype.mouseMove = function (event) {
        this.gesture.mouseMoved(event);
    };
    AppComponent.prototype.click = function (event) {
        this.gesture.appClicked(event);
    };
    return AppComponent;
}());
__decorate([
    core_1.ViewChild(navigation_component_1.NavigationComponent),
    __metadata("design:type", navigation_component_1.NavigationComponent)
], AppComponent.prototype, "appNav", void 0);
__decorate([
    core_1.ViewChild(audio_player_component_1.AudioPlayerComponent),
    __metadata("design:type", audio_player_component_1.AudioPlayerComponent)
], AppComponent.prototype, "audioPlayer", void 0);
AppComponent = __decorate([
    core_1.Component({
        selector: 'my-app',
        template: "\n        <div id=\"layout-container\" (mousemove)=\"mouseMove($event)\" (click)=\"click($event)\">\n            <router-outlet></router-outlet>\n            <app-navigation (mouseenter)=\"mouseEnterDecoration()\" (mouseleave)=\"mouseLeaveDecoration()\"></app-navigation>\n            <audio-player (mouseenter)=\"mouseEnterDecoration()\" (mouseleave)=\"mouseLeaveDecoration()\"></audio-player>\n        </div>\n    ",
        styleUrls: ['app/app.component.css']
    }),
    __param(0, core_1.Inject(igesture_1.I_GESTURE)),
    __param(1, core_1.Inject(ilogger_1.I_LOGGER)),
    __metadata("design:paramtypes", [Object, Object, core_1.Renderer,
        mediacheck_service_1.MediacheckService])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map