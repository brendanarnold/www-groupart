
import { Subscription } from 'rxjs/Subscription';
import { Component, Inject, Input, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { IGesture, I_GESTURE } from './services/gesture/igesture';

export class Link {
    constructor(public route = "", public text = "") {}
}

@Component({
    moduleId: module.id,
    selector: 'dropdown-menu',
    styles: [`
    .dropdown-menu {
    }
    .title-container {
        display: inline-block;
    }
    .dropdown-anchor {
        position: relative;
        height: 0px;
    }
    ul {
        position: absolute;
        top: 10px;
        left: -10px;
        background-color: #fff;
        list-style-type: none;
        padding-bottom: 10px;
    }
    ul > li {
        padding: 10px 10px 0px 10px;
    }
    ul > li > a {
    }
    .isopen {
        color: red;
    }
    `],
    template: `
<div class="dropdown-menu" #root>
    <a [class.isopen]="isVisible" class="hyperlink title-container" (click)="toggleVisibility()">{{ text }}</a>
    <div (mouseleave)="mouseLeave()" class="dropdown-anchor">
        <ul *ngIf="isVisible" >
            <li *ngFor="let link of links"><a class="hyperlink" routerLinkActive="active" routerLink="{{ link.route }}">{{ link.text }}</a></li>
        </ul>
    </div>
 </div>`
})
export class DropdownMenuComponent implements OnDestroy {

    isVisible: boolean = false;
    subscriptions: Subscription[] = [];

    @ViewChild("root") root;
    @Input() text: string = "";
    @Input() links: Link[] = [];

    constructor(@Inject(I_GESTURE) private gesture: IGesture) { 
        this.subscriptions.push(this.gesture.appClicked$.subscribe((event) => this.appClicked(event)));
    }

    appClicked(event: MouseEvent) {
        // If click outside of the menu when open, close the menu
        if (this.isVisible && !this.root.nativeElement.contains(event.target)) {
            this.isVisible = false;
        }
    }

    mouseLeave() {
        //this.isVisible = false;
    }

    toggleVisibility() {
        this.isVisible = !this.isVisible;
    }

    ngOnDestroy() { 
        for (let sub of this.subscriptions) { 
            sub.unsubscribe();
        }
    }
}
