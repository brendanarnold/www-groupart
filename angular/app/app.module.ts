import './rxjs-extensions';

import { NgModule, OpaqueToken } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule, Title } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppComponent }   from './app.component';
import { HomeComponent } from './home/home.component';
import { WorksComponent } from './works/works.component';
import { WorksTowerComponent } from './works/works-tower.component';
import { WorksManifestationsComponent } from './works/works-manifestations.component';
import { WorksRecursionsComponent } from './works/works-recursions.component';
import { AboutComponent } from './about/about.component';
import { AboutManifestoComponent } from './about/about-manifesto.component';
import { NavigationComponent } from './navigation.component';
import { DropdownMenuComponent } from './dropdown-menu.component';
import { AudioPlayerComponent } from './audio-player.component';
import { I_LOGGER } from './services/logger/ilogger';
import { ConsoleLogger } from './services/logger/console-logger.service';
import { I_AUDIO } from './services/audio/iaudio';
import { HtmlAudio } from './services/audio/html-audio.service';
import { HtmlGesture } from './services/gesture/html-gesture.service';
import { I_GESTURE } from './services/gesture/igesture';
import { AppRoutingModule } from './app-routing.module';
import { NewsModule } from './news/news.module';
import { TabComponent } from './tab.component';
import { TabsComponent } from './tabs.component';
import { CarouselComponent } from './carousel.component';
import { CarouselImageComponent } from './carousel-image.component';
import { CarouselDiptychComponent } from './carousel-diptych.component';
import { CarouselTriptychComponent } from './carousel-triptych.component';
import { ImageVariantPipe } from './image-variant.pipe';
import { MediacheckService } from './services/mediacheck/mediacheck.service';
import { PhpListSubscribeService } from './services/subscribe/phplist-subscribe.service';
import { I_SUBSCRIBE } from './services/subscribe/isubscribe';
import { TestComponent } from './test.component';

@NgModule({
    imports: [
        NewsModule,
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        WorksComponent,
        WorksTowerComponent,
        WorksManifestationsComponent,
        WorksRecursionsComponent,
        AboutComponent,
        AboutManifestoComponent,
        AudioPlayerComponent,
        NavigationComponent,
        DropdownMenuComponent,
        TabComponent,
        TabsComponent,
        CarouselComponent,
        CarouselImageComponent,
        CarouselDiptychComponent,
        CarouselTriptychComponent,
        ImageVariantPipe,
        TestComponent
    ],
    exports: [
    ],
    providers: [
        { provide: I_LOGGER, useClass: ConsoleLogger },
        { provide: I_AUDIO, useClass: HtmlAudio },
        { provide: I_GESTURE, useClass: HtmlGesture },
        { provide: I_SUBSCRIBE, useClass: PhpListSubscribeService },
        Title,
        MediacheckService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
