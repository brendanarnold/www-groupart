import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Title } from '@angular/platform-browser';

import { I_LOGGER, ILogger } from '../services/logger/ilogger';
import { I_AUDIO, IAudio } from '../services/audio/iaudio';

import { AUDIO_CLIPS } from '../services/audio/audio-clips';
import { AudioClip } from '../services/audio/audioclip';


@Component({
    moduleId: module.id,
    selector: 'home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

    subscriptions: Subscription[] = [];
    manifestoBtnText: string;
    
    constructor(@Inject(I_LOGGER) private logger: ILogger,
        @Inject(I_AUDIO) private audioService: IAudio,
        private titleService: Title) { 
        this.subscriptions.push(audioService.playbackChanged$.subscribe(() => { this.updateButton() }))            
        this.updateButton();
    }

    updateButton() {
        let clip = this.audioService.currentClip;
        let isPlaying = this.audioService.isPlaying;
        this.manifestoBtnText = (clip === AUDIO_CLIPS.manifesto && isPlaying) ? "Pause Audio" : "Play Manifesto";
    }

    ngOnInit() {
        this.titleService.setTitle("The Orm");
    }

    ngOnDestroy() {
        for (let sub of this.subscriptions) { sub.unsubscribe(); }
    }
    
    playManifesto() {
        this.audioService.togglePlayback(AUDIO_CLIPS.manifesto);
    }

}