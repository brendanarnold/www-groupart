"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var ilogger_1 = require("../services/logger/ilogger");
var iaudio_1 = require("../services/audio/iaudio");
var audio_clips_1 = require("../services/audio/audio-clips");
var HomeComponent = (function () {
    function HomeComponent(logger, audioService, titleService) {
        var _this = this;
        this.logger = logger;
        this.audioService = audioService;
        this.titleService = titleService;
        this.subscriptions = [];
        this.subscriptions.push(audioService.playbackChanged$.subscribe(function () { _this.updateButton(); }));
        this.updateButton();
    }
    HomeComponent.prototype.updateButton = function () {
        var clip = this.audioService.currentClip;
        var isPlaying = this.audioService.isPlaying;
        this.manifestoBtnText = (clip === audio_clips_1.AUDIO_CLIPS.manifesto && isPlaying) ? "Pause Audio" : "Play Manifesto";
    };
    HomeComponent.prototype.ngOnInit = function () {
        this.titleService.setTitle("The Orm");
    };
    HomeComponent.prototype.ngOnDestroy = function () {
        for (var _i = 0, _a = this.subscriptions; _i < _a.length; _i++) {
            var sub = _a[_i];
            sub.unsubscribe();
        }
    };
    HomeComponent.prototype.playManifesto = function () {
        this.audioService.togglePlayback(audio_clips_1.AUDIO_CLIPS.manifesto);
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'home',
        templateUrl: 'home.component.html',
        styleUrls: ['home.component.css']
    }),
    __param(0, core_1.Inject(ilogger_1.I_LOGGER)),
    __param(1, core_1.Inject(iaudio_1.I_AUDIO)),
    __metadata("design:paramtypes", [Object, Object, platform_browser_1.Title])
], HomeComponent);
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map