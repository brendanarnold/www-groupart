import { Input } from '@angular/core';

export class CarouselItemBase {
    @Input() active: boolean = false;
}