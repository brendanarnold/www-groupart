import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
    moduleId: module.id,
    selector: 'about-manifesto',
    templateUrl: 'about-manifesto.component.html',
    styleUrls: ['about-manifesto.component.css']
})
export class AboutManifestoComponent implements OnInit {
    constructor(private titleService: Title) { }

    ngOnInit() { 
        this.titleService.setTitle("Manifesto | The Orm")
    }
}