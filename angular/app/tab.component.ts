import { Component, Input } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'tab',
    template: `
    <div [hidden]="!active" class="root-container">
        <ng-content></ng-content>
    </div>
    `,
    styles: [``]
})
export class TabComponent {

    @Input('tabId') tabId: string;
    @Input('tabTitle') title: string;
    @Input() active: boolean = false;

}