/* Adapted from https://github.com/kmaida/angular2-mediacheck */
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var MediacheckService = (function () {
    function MediacheckService(zone) {
        this.zone = zone;
        this.mqueries = {
            small: '(max-width: 760px)',
            large: '(min-width: 761px)'
        };
    }
    MediacheckService.prototype.check = function (mqName) {
        if (!this.mqueries[mqName]) {
            console.warn("No media query registered for \"" + mqName + "\"!");
        }
        return window.matchMedia(this.mqueries[mqName]).matches;
    };
    Object.defineProperty(MediacheckService.prototype, "getMqName", {
        get: function () {
            for (var key in this.mqueries) {
                if (window.matchMedia(this.mqueries[key]).matches) {
                    return key;
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    MediacheckService.prototype.onMqChange = function (mqName, callback) {
        var self = this;
        if (typeof callback === 'function') {
            var mql = window.matchMedia(this.mqueries[mqName]);
            // if listener is already in list, this has no effect
            mql.addListener(function (mql) {
                self.zone.run(function () {
                    if (mql.matches) {
                        callback(mql);
                    }
                });
            });
        }
        else {
            console.warn("No valid callback available for \"" + mqName + "\"!");
        }
    };
    return MediacheckService;
}());
MediacheckService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [core_1.NgZone])
], MediacheckService);
exports.MediacheckService = MediacheckService;
//# sourceMappingURL=mediacheck.service.js.map