
import { OpaqueToken } from '@angular/core';
import { Observable } from 'rxjs/Observable';


export interface IGesture {
    
    appClicked$: Observable<MouseEvent>;
    mouseMoved$: Observable<MouseEvent>;
    windowScrolled$: Observable<UIEvent>;
    
    mouseMoved(event: MouseEvent);
    appClicked(event: MouseEvent);
    windowScrolled(event: UIEvent);
}

export let I_GESTURE = new OpaqueToken('IGesture');