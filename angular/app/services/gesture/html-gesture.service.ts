import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Rx';
import { I_GESTURE, IGesture } from './igesture';
import { DOCUMENT } from '@angular/platform-browser';

@Injectable()
export class HtmlGesture implements IGesture {

    private mouseMovedSource = new Subject<MouseEvent>();
    private appClickedSource = new Subject<MouseEvent>();
    private windowScrolledSource = new Subject<UIEvent>();

    mouseMoved$ = this.mouseMovedSource.asObservable();
    appClicked$ = this.appClickedSource.asObservable();
    windowScrolled$ = this.windowScrolledSource.asObservable();

    constructor() { }

    mouseMoved(event: MouseEvent) {
        this.mouseMovedSource.next(event);
    
    }

    appClicked(event: MouseEvent) {
        this.appClickedSource.next(event);
    }

    windowScrolled(event: UIEvent) {
        this.windowScrolledSource.next(event);
    }
    

}