import { Injectable, Inject } from '@angular/core';
import { IAudio } from './iaudio';
import { ILogger, I_LOGGER } from './../logger/ilogger';
import { Subject } from 'rxjs/Subject';
import { AudioClip } from './audioclip';
import { PlaybackState } from './playback-state';

@Injectable()
export class HtmlAudio implements IAudio {

    private playbackPausedSource = new Subject<null>();
    private playbackStartedSource = new Subject<null>();
    private playbackStoppedSource = new Subject<null>();
    private playbackChangedSource = new Subject<null>();
    private playbackTimeChangedSource = new Subject<null>();
    private newClipLoadedSource = new Subject<null>();

    playbackPaused$ = this.playbackPausedSource.asObservable();
    playbackStarted$ = this.playbackStartedSource.asObservable();
    playbackStopped$ = this.playbackStoppedSource.asObservable();
    playbackChanged$ = this.playbackChangedSource.asObservable();
    playbackTimeChanged$ = this.playbackTimeChangedSource.asObservable();
    newClipLoaded$ = this.newClipLoadedSource.asObservable();

    playbackState: PlaybackState = "stopped";
    currentClip: AudioClip = null;
    playbackTime: number = 0.0;
    get isPlaying(): boolean {
        return this.playbackState === "playing";
    }

    constructor(@Inject(I_LOGGER) private logger: ILogger) { }

    timeUpdated(newTime: number) {
        this.playbackTime = newTime;
        this.playbackTimeChangedSource.next();
    }

    togglePlayback(clip: AudioClip) {
        if (this.currentClip !== clip) {
            this.stop();
            this.play(clip);
        } else if (this.isPlaying) {
            this.pause();            
        } else {
            this.play(clip);
        }
    }

    loadNewClip(clip: AudioClip) {
        if (this.currentClip === clip) { return; }
        this.logger.trace("New clip loaded: " + (clip && clip.filename));
        this.currentClip = clip;
        this.newClipLoadedSource.next();
    }

    play(clip: AudioClip) {
        if (this.isPlaying && this.currentClip === clip) { return; }
        this.logger.trace("Started playback");
        this.playbackState = "playing";
        if (this.currentClip !== clip) {
            this.loadNewClip(clip);
        }
        this.playbackStartedSource.next();
        this.playbackChangedSource.next();
    }

    pause() {
        if (this.playbackState === "paused") { return }
        this.logger.trace("Paused playback");
        this.playbackState = "paused";
        this.playbackPausedSource.next();
        this.playbackChangedSource.next();
    }

    stop() {
        if (this.playbackState === "stopped") { return }
        this.logger.trace("Stopped playback");
        this.currentClip = null;
        this.playbackState = "stopped";
        this.playbackStoppedSource.next();
        this.playbackChangedSource.next();
    }
}