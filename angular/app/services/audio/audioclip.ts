export class AudioClip {

    constructor(public name: string, public filename: string, public duration: number) {}
}