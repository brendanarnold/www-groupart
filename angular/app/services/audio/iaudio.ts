import { OpaqueToken } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AudioClip } from './audioclip';
import { PlaybackState } from './playback-state';

export interface IAudio {
    playbackPaused$: Observable<null>;
    playbackStopped$: Observable<null>;
    playbackStarted$: Observable<null>;
    playbackChanged$: Observable<null>;
    playbackTimeChanged$: Observable<null>;
    newClipLoaded$: Observable<null>;

    playbackState: PlaybackState;
    isPlaying: boolean;
    currentClip: AudioClip;
    playbackTime: number;

    timeUpdated(newTime: number): void;
    togglePlayback(clip: AudioClip): void;
    play(clip: AudioClip): void;
    pause(): void;
    stop(): void;
}

export let I_AUDIO = new OpaqueToken('IAudio');
