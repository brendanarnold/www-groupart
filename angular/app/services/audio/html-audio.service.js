"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var ilogger_1 = require("./../logger/ilogger");
var Subject_1 = require("rxjs/Subject");
var HtmlAudio = (function () {
    function HtmlAudio(logger) {
        this.logger = logger;
        this.playbackPausedSource = new Subject_1.Subject();
        this.playbackStartedSource = new Subject_1.Subject();
        this.playbackStoppedSource = new Subject_1.Subject();
        this.playbackChangedSource = new Subject_1.Subject();
        this.playbackTimeChangedSource = new Subject_1.Subject();
        this.newClipLoadedSource = new Subject_1.Subject();
        this.playbackPaused$ = this.playbackPausedSource.asObservable();
        this.playbackStarted$ = this.playbackStartedSource.asObservable();
        this.playbackStopped$ = this.playbackStoppedSource.asObservable();
        this.playbackChanged$ = this.playbackChangedSource.asObservable();
        this.playbackTimeChanged$ = this.playbackTimeChangedSource.asObservable();
        this.newClipLoaded$ = this.newClipLoadedSource.asObservable();
        this.playbackState = "stopped";
        this.currentClip = null;
        this.playbackTime = 0.0;
    }
    Object.defineProperty(HtmlAudio.prototype, "isPlaying", {
        get: function () {
            return this.playbackState === "playing";
        },
        enumerable: true,
        configurable: true
    });
    HtmlAudio.prototype.timeUpdated = function (newTime) {
        this.playbackTime = newTime;
        this.playbackTimeChangedSource.next();
    };
    HtmlAudio.prototype.togglePlayback = function (clip) {
        if (this.currentClip !== clip) {
            this.stop();
            this.play(clip);
        }
        else if (this.isPlaying) {
            this.pause();
        }
        else {
            this.play(clip);
        }
    };
    HtmlAudio.prototype.loadNewClip = function (clip) {
        if (this.currentClip === clip) {
            return;
        }
        this.logger.trace("New clip loaded: " + (clip && clip.filename));
        this.currentClip = clip;
        this.newClipLoadedSource.next();
    };
    HtmlAudio.prototype.play = function (clip) {
        if (this.isPlaying && this.currentClip === clip) {
            return;
        }
        this.logger.trace("Started playback");
        this.playbackState = "playing";
        if (this.currentClip !== clip) {
            this.loadNewClip(clip);
        }
        this.playbackStartedSource.next();
        this.playbackChangedSource.next();
    };
    HtmlAudio.prototype.pause = function () {
        if (this.playbackState === "paused") {
            return;
        }
        this.logger.trace("Paused playback");
        this.playbackState = "paused";
        this.playbackPausedSource.next();
        this.playbackChangedSource.next();
    };
    HtmlAudio.prototype.stop = function () {
        if (this.playbackState === "stopped") {
            return;
        }
        this.logger.trace("Stopped playback");
        this.currentClip = null;
        this.playbackState = "stopped";
        this.playbackStoppedSource.next();
        this.playbackChangedSource.next();
    };
    return HtmlAudio;
}());
HtmlAudio = __decorate([
    core_1.Injectable(),
    __param(0, core_1.Inject(ilogger_1.I_LOGGER)),
    __metadata("design:paramtypes", [Object])
], HtmlAudio);
exports.HtmlAudio = HtmlAudio;
//# sourceMappingURL=html-audio.service.js.map