"use strict";
var AudioClip = (function () {
    function AudioClip(name, filename, duration) {
        this.name = name;
        this.filename = filename;
        this.duration = duration;
    }
    return AudioClip;
}());
exports.AudioClip = AudioClip;
//# sourceMappingURL=audioclip.js.map