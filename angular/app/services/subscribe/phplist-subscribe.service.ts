import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/observable';
import { ISubscribe } from './isubscribe';

@Injectable()
export class PhpListSubscribeService implements ISubscribe {

    SUBSCRIBE_PAGE_ID = '1';
    LIST_ID = '2';

    constructor(private http: Http) { }

    subscribeToMailingList(email: string): Observable<void> {
        // Build the formdata needed for PhpList software
        let fd = new FormData();
        fd.append("email", email);
        fd.append("attribute1", "visitor");
        fd.append("htmlemail", "1");
        fd.append("list[" + this.LIST_ID + "]", "signup");
        fd.append("VerificationCodeX", "");
        fd.append("listname[" + this.LIST_ID + "]", "newsletter");
        fd.append("subscribe", "subscribe");
        // Build the post URL
        let postUrl = '/mailinglist/index.php?p=asubscribe&id=' + this.SUBSCRIBE_PAGE_ID;
        // Set the enctype
        //let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        return this.http.post(postUrl, fd, { headers: headers })
            .map(r => {
                if (r.status !== 200 || r.text() === "FAIL") {
                    throw "There was a problem, try again later";
                }
            });
    }

    

}