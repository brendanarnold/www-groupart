"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var PhpListSubscribeService = (function () {
    function PhpListSubscribeService(http) {
        this.http = http;
        this.SUBSCRIBE_PAGE_ID = '1';
        this.LIST_ID = '2';
    }
    PhpListSubscribeService.prototype.subscribeToMailingList = function (email) {
        // Build the formdata needed for PhpList software
        var fd = new FormData();
        fd.append("email", email);
        fd.append("attribute1", "visitor");
        fd.append("htmlemail", "1");
        fd.append("list[" + this.LIST_ID + "]", "signup");
        fd.append("VerificationCodeX", "");
        fd.append("listname[" + this.LIST_ID + "]", "newsletter");
        fd.append("subscribe", "subscribe");
        // Build the post URL
        var postUrl = '/mailinglist/index.php?p=asubscribe&id=' + this.SUBSCRIBE_PAGE_ID;
        // Set the enctype
        //let headers = new Headers({ 'Content-Type': 'multipart/form-data' });
        var headers = new http_1.Headers({ 'Content-Type': 'multipart/form-data' });
        return this.http.post(postUrl, fd, { headers: headers })
            .map(function (r) {
            if (r.status !== 200 || r.text() === "FAIL") {
                throw "There was a problem, try again later";
            }
        });
    };
    return PhpListSubscribeService;
}());
PhpListSubscribeService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], PhpListSubscribeService);
exports.PhpListSubscribeService = PhpListSubscribeService;
//# sourceMappingURL=phplist-subscribe.service.js.map