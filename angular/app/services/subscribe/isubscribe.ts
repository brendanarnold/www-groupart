
import { OpaqueToken } from '@angular/core';
import { Observable } from 'rxjs/Observable';


export interface ISubscribe {
    
    subscribeToMailingList(email: string): Observable<void>;
}

export let I_SUBSCRIBE = new OpaqueToken('ISubscribe');