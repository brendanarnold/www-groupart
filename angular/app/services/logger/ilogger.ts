
import { OpaqueToken } from '@angular/core';

export interface ILogger {
    trace(msg: string): void;
    warn(msg: string): void;
    error(msg: string): void;
}

export let I_LOGGER = new OpaqueToken('ILogger');
