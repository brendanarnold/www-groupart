"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var ConsoleLogger = (function () {
    function ConsoleLogger() {
    }
    ConsoleLogger.prototype.getTimestampString = function () {
        var d = new Date();
        var hrs = d.getUTCHours();
        var mns = d.getUTCMinutes();
        var scs = d.getUTCSeconds();
        var mss = d.getUTCMilliseconds();
        return hrs + ":" + mns + ":" + scs + "." + mss;
    };
    ConsoleLogger.prototype.trace = function (msg) {
        console.log("TRACE " + this.getTimestampString() + " - " + msg);
    };
    ConsoleLogger.prototype.warn = function (msg) {
        console.log("WARN " + this.getTimestampString() + " - " + msg);
    };
    ConsoleLogger.prototype.error = function (msg) {
        console.log("ERROR " + this.getTimestampString() + " - " + msg);
    };
    return ConsoleLogger;
}());
ConsoleLogger = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], ConsoleLogger);
exports.ConsoleLogger = ConsoleLogger;
//# sourceMappingURL=console-logger.service.js.map