import { Injectable } from '@angular/core';
import { ILogger } from './ilogger';


@Injectable()
export class ConsoleLogger implements ILogger {
    
    getTimestampString(): string {
        let d = new Date();
        let hrs = d.getUTCHours();
        let mns = d.getUTCMinutes();
        let scs = d.getUTCSeconds();
        let mss = d.getUTCMilliseconds();
        return hrs + ":" + mns + ":" + scs + "." + mss;
    }

    trace(msg: string) {
        console.log("TRACE " + this.getTimestampString() + " - " + msg);
    }
    
    warn(msg: string) {
        console.log("WARN " + this.getTimestampString() + " - " + msg);
    }
    
    error(msg: string) {
        console.log("ERROR " + this.getTimestampString() + " - " + msg);
    }
}