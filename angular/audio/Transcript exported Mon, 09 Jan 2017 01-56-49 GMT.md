00:08 Exist in now

00:14 What we can say most strongly is we're drawn to them, we find find them interesting because of how they are and we are not trying to rationalise that in that maybe a lot of other art does, that it tells you this is why, or this is what it means or this is the reason, we're simply saying 'it is'.

00:28 There's a lot of interaction is sculpture anyway, just from your natural approach to a piece. The way you walk around it, there's a discovery process,once you get closer, as you can see around the sculpture. We wanted to enhance that rather than do something entirely different.

00:49 _Muttering in studio_

01:10 Like when you approach the objects the light get brighter and it reveals more, the sounds overlay, become more intense, the more you see the more you hear

01:24 We used techniques from the theatre to bring out shadows ...

01:28 Maybe how poetry doesn't directly tell you something immediately but it paints a picture for you

01:54 There's the level of the artwork and there's the level of the meaning. A conceptual piece of artwork isn't interested in what it actually is, it's trying to direct you to something beyond the artwork itself. In many ways the way we are working is almost the opposite of that in that we are interested in what the artwork actually is and not giving any explicit meaning or not trying to tell something specific to the audience

02:17 To kind of bring you back to ...

02:20 Quite a lot of effort in new technologies are about removing us further from reality, like creating virtual realities, virtual ...

02:31 ... spaces that are like our minds

02:43 You know there is this kind of online space where all this information or stuff is. It's changed our thoughts of the world - the idea of the world we exist in now, because there is this other world attached to it which is just information floating around, that you can ...

03:01 ... parallel spaces ...

03:03 ... yeah ...

03:06  ... that we are increasingly living in

03:06 We create books and write things down, that is not what is actually in front of you, you read books to inhabit those parallel spaces instead of the space you're actually in now so its the same driver behind that kind thing that have something in common which is trying to inhabit a parallel world that means something else beyond just the world that you're sitting in right now staring at in front of you

03:29 Well you have dreams and thoughts which are also parallel worlds aren't they? And they make connection with real thing don't they? Your dreams, you connect them to, you connect objects together or object to feelings

03:46 Do you think it's necessary? We have to have these parallel worlds?

03:51 Its like an image though. If you paint a white horse it makes you think of a white horse but it actually doesn't make you think of paint. It removes the object - of what's in front of you - to an image

04:11 Exist in now

04:58 Now making something that is just there is quite an interesting thing to do because you are kind of creating something in a way that - you are just saying this needs to exist

05:16 The object is already there in front of you

05:23 It's totally different when you see them in your hand or you can play around with them. I mean there's a materiality to it - the textures - even the electronics. A lot of people when they have electronics in artworks, they're much more interested in what the electronics does and how it works as opposed to the materiality of the electronics. They are not really interested in seeing the circuitry ... there are a lot of works that hide the electronics away, so they are only interested in what it does. You quite often see that in the way a lot of electronic artworks are - the overall look of the piece, the direction of the piece - they are there just to implement, you know, use electronics know-how and make the light flash or make that thing move. The process of actually developing the board - not taking that out - in fact including this and more letting the board evolve rather than being so strongly, rigorously designed to a specification. A long time ago it would be the Swiss watch would show all its parts and you'd take delight in the mechanism itself.

06:46 People associate origins with sounds

06:48 Energetic pulsing breathing rhythmic sounds. Real sounds, modified real sounds and synthesised sounds

07:11 I'm trying to create sounds that are believable, that are interesting, that are complex but that you wouldn't encounter in the real world. Sounds that I've created from scratch or sounds that I've built up from fundamental components or they're recorded sounds but that have been manipulated and shaped and edited to a point that - that sound experience - you couldn't have just walking around, just listening to a piece of music. With the sound I try and achieve an experience that is different from the traditional musical experience. Music is generally is seen as a mode of expression or a way of transmitting a kind of emotion, a way or feeling - to try an tell you what you should be feeling, so the role of music in a film is to tell you what to feel, it's there to make you scared or emotional or whatever it is - it's trying to drag you in different directions and trying to make you feel things.

08:25 ... but as you go more abstract then you can start almost listening to the sounds without those causes and it can become a way towards a more pure sonic experience. In the same way you can run your hand against a rock and feel he grain of it, you can try to listen to a sound just for the actual qualities of a certain sound itself.

09:06 We naturally find things that we like in the natural world that weren't made by human beings. In the desert, the random processes that shift the sand dunes is going to end up making sand dunes that are all roughly similar to each other, it's never going to make a sandcastle, it's always going to make sand dunes ...

09:22 ... yeah ...

09:24 ... a human is going to make sandcastles and we are somewhere in-between those two, so we're using randomness but we are shaping and informing that randomness to create something that's neither a sand dune nor a sandcastle

09:39 There's natural in the sense that most people understand natural to mean which is probably like a rock or a tree or something and there's natural in the mathematical sense which is the most fundamental ...

09:48 ... as in a rock occurs natural but also to human beings, as an animal, it occurs naturally to them to draw circles and squares and things like that ...

09:56 ... a fundamental shape that anyone would come up with because it's 'square one' if you like. It's ...

10:02 ... just shapes that exist. Or simple geometrical shapes and symmetries or circles and squares.

10:19 Exist in now

10:38 _Footsteps_