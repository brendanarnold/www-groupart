

# An Angular 2 website

This is a personal website using Angular 2, previous versions used Gulp to compile static pages but this version uses Angular 2 served from a static webserver.

I added a couple of recent commits to tidy up and add this README.

